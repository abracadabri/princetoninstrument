# -*- coding: utf-8 -*-
"""
Created on Fri Mar 20 15:28:26 2015

@author: Helene
"""
from ctypes import Structure

class Region(ctype.Structure):
    _fields_ = [
        ("s1", c_int16),  #First pixel in the serial register
        ("s2", c_int16), #Last pixel in the serial register
        ("sbin", c_int16), #Serial binning for this region 
        ("p1", c_int16), #First row in the parallel register
        ("p2", c_int16), #Last row in the parallel register 
        ("pbin", c_int16)] #Parallel binning for this region 
        
frame_size=c_uint32(0)        
numberframes=5
status=c_int16(0)
not_needed=c_uint32(0)
adress=c_void_p()
lib.pl_exp_init_seq()
lib.pl_exp_setup_cont(c_int16(handle),1,POINTER(region), Camera.EXPOSURE_MODE["TIMED_MODE"],100, byref(frame_size), Camera.CIRC_TYPE["CIRC_OVERWRITE"])
print("Frame Size:")
print(frame_size)
buffer_size= 3*frame_size
data_buffer=(np.empty(buffer_size, dtype=np.int16))   
data_buffer_Ptr = data_buffer.ctypes.data_as(POINTER(c_int16))
print( "Collecting %i Frames\n", numberframes )
lib.pl_exp_start_cont(c_int16(handle), data_buffer, buffer_size );

while numberframes > 0:
  
  status_flag= bool(lib.pl_exp_check_cont_status(c_int16(handle), byref(status), byref(not_needed), byref(not_needed)))
  
  while status_flag==True and status != Camera.CHECK_STATUS["READOUT_COMPLETE"] and status != Camera.CHECK_STATUS["READOUT_FAILED"] :

     if status == Camera.CHECK_STATUS["READOUT_FAILED"]:
       print("Data collection error: %i\n", lib.pl_error_code() )

     if bool(lib.pl_exp_get_latest_frame( c_int16(handle), adress )) == True: 
         # address now points to valid data */
         print( "Center Three Points: %i, %i, %i\n")
               #*((uns16*)address + frame_size/sizeof(uns16)/2 - 1),
               #*((uns16*)address + frame_size/sizeof(uns16)/2),
               #*((uns16*)address + frame_size/sizeof(uns16)/2 + 1) );
         numberframes=numberframes -1
         print( "Remaining Frames %i\n", numberframes )
         
#Stop the acquisition */
lib.pl_exp_stop_cont(c_int16(handle),Camera.CCS["CCS_HALT"])
#Finish the sequence */
lib.pl_exp_finish_seq(c_int16(handle), data_buffer_Ptr, 0)

#Uninit the sequence */
lib.pl_exp_uninit_seq()
#free( buffer );
