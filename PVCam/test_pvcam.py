# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 13:26:36 2015

@author: Helene
"""
from __future__ import division
from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals
import time
import math
# to load the proper dll
import platform
import PrincetonInstrument as PI
import numpy as np
# Do not import or use ill definied data types
# such as short int or long
# use the values specified in the h file
# float is always defined as 32 bits
# double is defined as 64 bits
from ctypes import byref, POINTER, create_string_buffer, c_float, \
    c_int16, c_int32, c_uint32, c_void_p, CFUNCTYPE, c_char_p, c_char, c_bool, addressof
from ctypes import c_int32 as c_enum
from ctypes import windll

from ctypes import Structure

class rgn_type(Structure):
    _fields_ = [
        ("s1", c_int16),  #First pixel in the serial register
        ("s2", c_int16), #Last pixel in the serial register
        ("sbin", c_int16), #Serial binning for this region 
        ("p1", c_int16), #First row in the parallel register
        ("p2", c_int16), #Last row in the parallel register 
        ("pbin", c_int16)] #Parallel binning for this region 
    
if __name__ == "__main__":
    print(__doc__)
    
    Camera=PI.PrincetonInstrumentCCD()
    LIBNAME="Pvcam32"
    lib = windll.LoadLibrary("C:\Program Files\Princeton Instruments\WinSpec\Pvcam32.dll")
    print(lib)
    cam_name=create_string_buffer(4)
      
    total_cams=10
    handle=0
    version=1
    c_total_cams=c_int16(total_cams)
    print("Initializes camera:")
    #print(c_total_cams)
    print(lib.pl_pvcam_init())
    print("pl_pvcam_init error code:")
    print(lib.pl_error_code())
    #print(lib.pl_cam_get_name(c_int16(0), c_char_p(cam_name) ))
    print("number of cameras before")    
    print(c_total_cams)    
    print(lib.pl_cam_get_total(byref(c_total_cams)))
    print("number of cameras after")    
    print(c_total_cams)
    print("pl_cam_get_total error code:")
    print(lib.pl_error_code())
    
    print("Try pl_cam_get_name:")
    print(lib.pl_cam_get_name(c_int16(0),cam_name))
    print(cam_name)
    print("pl_cam_get_name error code:")
    print(lib.pl_error_code())
    
    print("Is this the handle of an open camera?")
    print(lib.pl_cam_check(c_int16(handle)))
    print("Try pl_cam_open:")
    print(lib.pl_cam_open(cam_name,byref(c_int16(handle)), c_int16(0)))
    print("pl_cam_open error code:")
    print(lib.pl_error_code())
    print("And now is this the handle of an open camera?")
    print(lib.pl_cam_check(c_int16(handle)))
    #print("pl_cam_open error code:")
    #print(lib.pl_error_code())

    #Checks for circular buffer support
    attr_avail=True
    print("checks for circular buffer support")
    function_flag=(bool(lib.pl_get_param(c_int16(handle),c_uint32(Camera.PARAM_CIRC_BUFFER), c_int16(Camera.PARAM_ATTR["ATTR_AVAIL"]), byref(c_bool(attr_avail))) ))   
    print(attr_avail)
    
    if function_flag == True and attr_avail == True:
        
        region=rgn_type(0,511,1,0,511,1)
        
        #region(0)
        #print(region(0))
        frame_size=1      
        numberframes=5
        status=c_int16(0)
        not_needed=c_uint32(0)
        adress=c_void_p()
        adress_ptr=POINTER(c_int16)
        lib.pl_exp_init_seq()
        print("pl_exp_init_seq error message:")
        print(lib.pl_error_code())
        lib.pl_exp_setup_cont(c_int16(handle),c_int16(1),byref(region), c_int16(Camera.EXPOSURE_MODE["TIMED_MODE"]),c_uint32(100), byref(c_uint32(frame_size)), c_int16(Camera.CIRC_TYPE["CIRC_OVERWRITE"]))
        print("pl_exp_setup_cont error message:")
        print(lib.pl_error_code())
        print("Frame Size:")
        print(frame_size)
        buffer_size= 3*frame_size
        print("Buffer size:")
        print(buffer_size)
        data_buffer=(np.empty(buffer_size, dtype=np.int16))   
        data_buffer_Ptr = data_buffer.ctypes.data_as(POINTER(c_int16))
        print( "Collecting %i Frames\n", numberframes )
        lib.pl_exp_start_cont(c_int16(handle), data_buffer_Ptr, c_uint32(buffer_size));
        print("pl_exp_start_cont error message:")
        print(lib.pl_error_code())
        while numberframes > 0:
  
          status_flag= bool(lib.pl_exp_check_cont_status(c_int16(handle), byref(status), byref(not_needed), byref(not_needed)))
  
          #while status_flag==True and status != Camera.CHECK_STATUS["READOUT_COMPLETE"] and status != Camera.CHECK_STATUS["READOUT_FAILED"] :
          #   print("Get latest frame status")
             #print(bool(lib.pl_exp_get_latest_frame( c_int16(handle), adress_ptr)))
          #   if status != Camera.CHECK_STATUS["READOUT_FAILED"]:
          #       print("Data collection error: %i\n", lib.pl_error_code() )

             #if bool(lib.pl_exp_get_latest_frame( c_int16(handle), adress )) == True: 
                 # address now points to valid data */
          #       print( "Center Three Points: %i, %i, %i\n")
               #*((uns16*)address + frame_size/sizeof(uns16)/2 - 1),
               #*((uns16*)address + frame_size/sizeof(uns16)/2),
               #*((uns16*)address + frame_size/sizeof(uns16)/2 + 1) );
          numberframes=numberframes -1
          print( "Remaining Frames %i\n", numberframes )
         
        #Stop the acquisition */
        lib.pl_exp_stop_cont(c_int16(handle),Camera.CCS["CCS_HALT"])
        print("pl_exp_stop_cont error message:")
        print(lib.pl_error_code())
        #Finish the sequence */
        lib.pl_exp_finish_seq(c_int16(handle), data_buffer_Ptr, 0)
        print("pl_exp_finish_seq error message:")
        print(lib.pl_error_code())
        #Uninit the sequence */
        lib.pl_exp_uninit_seq()
        print("pl_exp_uninit error message:")
        print(lib.pl_error_code())
        #free( buffer );
    else:
        print("Circular Buffer mode not supported")
        
    print("try pl_cam_get_diags")
    print(lib.pl_cam_get_diags(c_int16(handle)))
    print("pl_cam_get_diags error code:")
    print(lib.pl_error_code())
    
    attr_avail=c_bool(0)
    print("Attr available before:")    
    print(attr_avail)
    print("try pl_get_param with ATTR_AVAIL")
    print(lib.pl_get_param(c_int16(handle),c_uint32(Camera.PARAM_READOUT_TIME), c_int16(Camera.PARAM_ATTR["ATTR_AVAIL"]), byref(attr_avail)))    
    print("pl_cam_get_param error code:")
    print(lib.pl_error_code())
    print("Attr available after:")
    print(attr_avail)    
    
    attr_access=c_int16(0)
    print("Attr access before:")    
    print(attr_access)
    print("try pl_get_param with ATTR_ACCESS")
    print(lib.pl_get_param(c_int16(handle),c_uint32(Camera.PARAM_READOUT_TIME), c_int16(Camera.PARAM_ATTR["ATTR_ACCESS"]), byref(attr_access))) 
    print("pl_cam_get_param error code:")
    print(lib.pl_error_code())    
    print("Attr access after:")    
    print(attr_access)
    
    attr_type=c_int16(0)
    print("Attr type before:")    
    print(attr_type)
    print("try pl_get_param with ATTR_TYPE")
    print(lib.pl_get_param(c_int16(handle),c_uint32(Camera.PARAM_READOUT_TIME), c_int16(Camera.PARAM_ATTR["ATTR_TYPE"]), byref(attr_type))) 
    print("pl_cam_get_param error code:")
    print(lib.pl_error_code())    
    print("Attr type after:")    
    print(attr_type)
    
    attr_current=c_float(0)
    print("Attr current before:")    
    print(attr_current)
    print("try pl_get_param with ATTR_CURRENT")
    print(lib.pl_get_param(c_int16(handle),c_uint32(Camera.PARAM_READOUT_TIME), c_int16(Camera.PARAM_ATTR["ATTR_CURRENT"]), byref(attr_current))) 
    print("pl_cam_get_param error code:")
    print(lib.pl_error_code())    
    print("Attr current after:")    
    print(attr_current)
    print("try pl_set_param with ATTR_CURRENT")
    print(lib.pl_get_param(c_int16(handle),c_uint32(Camera.PARAM_READOUT_TIME), c_int16(Camera.PARAM_ATTR["ATTR_CURRENT"]), byref(attr_current))) 
    print("pl_cam_get_param error code:")
    print(lib.pl_error_code())    
    print("Attr current after:")    
    
    
    
    print("try pl_cam_close:")
    print(lib.pl_cam_close(c_int16(handle)))
    print("pl_cam_close error code:")
    print(lib.pl_error_code())
    
    print(lib.pl_pvcam_uninit())
    """
    print(lib.pl_ddi_get_ver(byref(c_int16(version))))
    print(c_int16(version))
    print(lib.pl_cam_get_diags(c_int16(handle)))
    print(lib.pl_error_code())
    
    #print(cam_name)
    #print("unitiliaze the camera")
    #print(lib.pl_pvcam_uninit())
        
    print(lib.pl_cam_get_name(c_int16(0), c_char_p(cam_name) ))
    print(lib.pl_error_code())
    #print(lib.pl_cam_get_total(byref(c_total_cams)))
    #print(c_total_cams)
    print("Is this the handle of an open camera?")
    print(lib.pl_cam_check(c_int16(handle)))
    print("Is the camera open now?")
    print(lib.pl_cam_open(c_char_p(cam_name),byref(c_int16(handle)), c_int16(0)))
    print("Is this the handle of an open camera now?")
    print(lib.pl_cam_check(c_int16(handle)))
    """
    
    """ 
    char cam_name[CAM_NAME_LEN]; /* camera name */
    int16 hCam; /* camera handle */
     /* Initialize the PVCam Library and Open the First Camera */
     lib.pl_pvcam_init();
     pl_cam_get_name( 0, cam_name );
     pl_cam_open(cam_name, &hCam, OPEN_EXCLUSIVE );
     printf( "\nAnti_Blooming\n");
     DisplayParamIdInfo (hCam, PARAM_ANTI_BLOOMING);
     printf( "\nLogic Output\n");
     DisplayParamIdInfo (hCam, PARAM_LOGIC_OUTPUT);
     printf( "\nEdge Trigger\n");
     DisplayParamIdInfo (hCam, PARAM_EDGE_TRIGGER);
     printf( "\nIntensifier Gain\n");
     DisplayParamIdInfo (hCam, PARAM_INTENSIFIER_GAIN);
     printf( "\nGate Mode\n");
     DisplayParamIdInfo (hCam, PARAM_SHTR_GATE_MODE);
     printf( "\nMin Block\n");
     DisplayParamIdInfo (hCam, PARAM_MIN_BLOCK);
     printf( "\nNum Min Block\n");
     DisplayParamIdInfo (hCam, PARAM_NUM_MIN_BLOCK);
     printf( "\nStrips Per Clean\n");
    114 PVCAM Manual Version 2.7.D
     DisplayParamIdInfo (hCam, PARAM_NUM_OF_STRIPS_PER_CLR);
     printf( "\nReadout Port\n");
     DisplayParamIdInfo (hCam, PARAM_READOUT_PORT);
     printf( "\nController Alive\n");
     DisplayParamIdInfo (hCam, PARAM_CONTROLLER_ALIVE);
     printf( "\nReadout Time\n");
     DisplayParamIdInfo (hCam, PARAM_READOUT_TIME);
     printf( "\nCircular Buffer Supp
     """