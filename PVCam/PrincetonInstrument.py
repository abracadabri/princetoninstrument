# -*- coding: utf-8 -*-
"""This is the low level driver file for interacting with thte Princeton."""

from __future__ import division
from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals

__author__ = "Colin O'Flynn, Mark Harfouche"
__license__ = "FreeBSD"

import sys

import inspect
import time
#import warnings

import numpy as np
from PyQt4 import QtGui, QtCore


class PrincetonInstrumentCCD(object):

    LIBNAME="Pvcam32"    
    """   
    #Dictionary with types used by pl_get_param with attribute type (ATTR_TYPE)
    TYPE_DICT= {"TYPE_CHAR_PTR":13,
                "TYPE_INT8":12,
                "TYPE_UNS8":5,
                "TYPE_INT16":1,
                "TYPE_UNS16":6,
                "TYPE_INT32":2,
                "TYPE_UNS32":7,
                "TYPE_UNS64":8,
                "TYPE_FLT64":4,
                "TYPE_ENUM":9,
                "TYPE_BOOLEAN":11,
                "TYPE_VOID_PTR":14,
                "TYPE_VOID_PTR_PTR":15}    
    """
    """
    # Dictionary with classes
    CLASSES_DICT = {"CLASS0":0, #Camera Communications
                "CLASS1":1, #Error reporting
                "CLASS2":2, #Configuration/setup
                "CLASS3":3, #Data Acquisition
                "CLASS4":4, #Buffer Manipulation
                "CLASS5":5, #Analysis
                "CLASS6":6, #Data Export
                "CLASS29":29, #Buffer Functions
                "CLASS30":30, #Utility Functions
                "CLASS31":31, #Memory Functions
                "CLASS32":32, #CCL Engine
                "CLASS91":91, #RS170
                "CLASS92":92, #Defect Mapping
                "CLASS93":93, #Fast Frame Operations (PIV/ACCUM/Kinetics)
                "CLASS94":94, #PIG
                "CLASS95":95, #Virtual Chip
                "CLASS96":96, #Acton Diagnostics
                "CLASS97":97, #Custom Chip
                "CLASS98":98, #Custom Timing
                "CLASS99":99 } #Trenton Diagnostics    
                
    """
    
    TYPE_CHAR_PTR=13
    TYPE_INT8=12
    TYPE_UNS8=5
    TYPE_INT16=1
    TYPE_UNS16=6
    TYPE_INT32=2
    TYPE_UNS32=7
    TYPE_UNS64=8
    TYPE_FLT64=4
    TYPE_ENUM=9
    TYPE_BOOLEAN=11
    TYPE_VOID_PTR=14
    TYPE_VOID_PTR_PTR=15
    
    
    CLASS0=0 #Camera Communications
    CLASS1=1 #Error reporting
    CLASS2=2 #Configuration/setup
    CLASS3=3 #Data Acquisition
    CLASS4=4 #Buffer Manipulation
    CLASS5=5 #Analysis
    CLASS6=6 #Data Export
    CLASS29=29 #Buffer Functions
    CLASS30=30 #Utility Functions
    CLASS31=31 #Memory Functions
    CLASS32=32 #CCL Engine
    CLASS91=91 #RS170
    CLASS92=92 #Defect Mapping
    CLASS93=93 #Fast Frame Operations (PIV/ACCUM/Kinetics)
    CLASS94=94 #PIG
    CLASS95=95 #Virtual Chip
    CLASS96=96 #Acton Diagnostics
    CLASS97=97 #Custom Chip
    CLASS98=98 #Custom Timing
    CLASS99=99 #Trenton Diagnostics
                
    # DEVICE DRIVER PARAMETERS
    #Class 0 parameters (next available index for class zero = 6) 
    PARAM_DD_INFO_LENGTH = (CLASS0<<16) + (TYPE_INT16<<24) +1
    PARAM_DD_VERSION = (CLASS0<<16) + (TYPE_UNS16<<24) +2
    PARAM_DD_RETRIES = (CLASS0<<16) + (TYPE_UNS16<<24) +3
    PARAM_DD_TIMEOUT = (CLASS0<<16) + (TYPE_UNS16<<24) +4
    PARAM_DD_INFO = (CLASS0<<16) + (TYPE_CHAR_PTR<<24) +5

    #Class 2 parameters (next available index for class two = 544)
    #CCD skip parameters                                                       
    #Min Block. amount to group on the shift register, to through way.         
    PARAM_MIN_BLOCK= ((CLASS2<<16) + (TYPE_INT16<<24) +  60)
    #number of min block groups to use before valid data.                      
    PARAM_NUM_MIN_BLOCK = ((CLASS2<<16) + (TYPE_INT16<<24) +  61)
    #number of strips to clear at one time, before going to the                
    #minblk/numminblk scheme                                                   
    PARAM_SKIP_AT_ONCE_BLK = ((CLASS2<<16) + (TYPE_INT32<<24) + 536)
    #Strips per clear. Used to define how many clears to use for continous     
    #clears and with clears to define the clear area at the beginning of an    
    #experiment.                                                               
    PARAM_NUM_OF_STRIPS_PER_CLR = ((CLASS2<<16) + (TYPE_INT16<<24) +  98)
    #Set Continuous Clears for Trenton Cameras. This is for clearing while     
    #in external trigger.                                                      
    PARAM_CONT_CLEARS = ((CLASS2<<16) + (TYPE_BOOLEAN<<24) + 540)   
    #Only applies to Thompson ST133 5Mhz                                       
    #enables or disables anti-blooming.                                        
    PARAM_ANTI_BLOOMING = ((CLASS2<<16) + (TYPE_ENUM<<24) + 293)
    #This applies to ST133 1Mhz and 5Mhz and PentaMax V5 controllers. For the  
    #ST133 family this controls whether the BNC (not scan) is either not scan  
    #or shutter for the PentaMax V5, this can be not scan, shutter, not ready, 
    #clearing, logic 0, logic 1, clearing, and not frame transfer image shift. 
    #See enum below for possible values                                        
    PARAM_LOGIC_OUTPUT =((CLASS2<<16) + (TYPE_ENUM<<24) +  66)
    #Edge Trigger defines whether the external sync trigger is positive or     
    #negitive edge active. This is for the ST133 family (1 and 5 Mhz) and      
    #PentaMax V5.0.                                                            
    #see enum below for possible values.                                       
    PARAM_EDGE_TRIGGER =((CLASS2<<16) + (TYPE_ENUM<<24) + 106)
    #Intensifier gain is currently only used by the PI-Max and has a range of  
    #0-255                                                                     
    PARAM_INTENSIFIER_GAIN =((CLASS2<<16) + (TYPE_INT16<<24) + 216)
    #Shutter, Gate, or Safe mode, for the PI-Max.                              
    PARAM_SHTR_GATE_MODE = ((CLASS2<<16) + (TYPE_ENUM<<24) + 217)
    #ADC offset setting.                                                       
    PARAM_ADC_OFFSET=((CLASS2<<16) + (TYPE_INT16<<24) + 195)
    #CCD chip name.
    PARAM_CHIP_NAME= ((CLASS2<<16) + (TYPE_CHAR_PTR<<24) + 129)
    PARAM_COOLING_MODE=((CLASS2<<16) + (TYPE_ENUM<<24) + 214)
    PARAM_PREAMP_DELAY=((CLASS2<<16) + (TYPE_UNS16<<24) + 502)
    PARAM_PREFLASH=((CLASS2<<16) + (TYPE_UNS16<<24) + 503)
    PARAM_COLOR_MODE=((CLASS2<<16) + (TYPE_ENUM<<24) + 504)
    PARAM_MPP_CAPABLE=((CLASS2<<16) + (TYPE_ENUM<<24) + 224)
    PARAM_PREAMP_OFF_CONTROL=((CLASS2<<16) + (TYPE_UNS32<<24) + 507)
    PARAM_SERIAL_NUM=((CLASS2<<16) + (TYPE_UNS16<<24) + 508)

    #CCD Dimensions and physical characteristics                               
    #pre and post dummies of CCD.                                              
    PARAM_PREMASK=((CLASS2<<16) + (TYPE_UNS16<<24) +  53)
    PARAM_PRESCAN=((CLASS2<<16) + (TYPE_UNS16<<24) +  55)
    PARAM_POSTMASK=((CLASS2<<16) + (TYPE_UNS16<<24) +  54)
    PARAM_POSTSCAN=((CLASS2<<16) + (TYPE_UNS16<<24) +  56)
    PARAM_PIX_PAR_DIST=((CLASS2<<16) + (TYPE_UNS16<<24) + 500)
    PARAM_PIX_PAR_SIZE=((CLASS2<<16) + (TYPE_UNS16<<24) +  63)
    PARAM_PIX_SER_DIST=((CLASS2<<16) + (TYPE_UNS16<<24) + 501)
    PARAM_PIX_SER_SIZE=((CLASS2<<16) + (TYPE_UNS16<<24) +  62)
    PARAM_SUMMING_WELL=((CLASS2<<16) + (TYPE_BOOLEAN<<24) + 505)
    PARAM_FWELL_CAPACITY=((CLASS2<<16) + (TYPE_UNS32<<24)  + 506)
    #Y dimension of active area of CCD chip
    PARAM_PAR_SIZE = ((CLASS2<<16) + (TYPE_UNS16<<24) +  57)
    #X dimension of active area of CCD chip 
    PARAM_SER_SIZE = ((CLASS2<<16) + (TYPE_UNS16<<24) +  58)    
    #X dimension of active area of CCD chip
    PARAM_ACCUM_CAPABLE = ((CLASS2<<16) + (TYPE_BOOLEAN<<24) + 538)
    PARAM_FTSCAN= ((CLASS2<<16) + (TYPE_UNS16<<24) + 59) 

    #customize chip dimension 
    PARAM_CUSTOM_CHIP= ((CLASS2<<16) + (TYPE_BOOLEAN<<24) +  87)   

    #customize chip timing
    PARAM_CUSTOM_TIMING= ((CLASS2<<16) + (TYPE_BOOLEAN<<24) +  88)
    PARAM_PAR_SHIFT_TIME=((CLASS2<<16) + (TYPE_UNS32<<24) + 545)
    PARAM_SER_SHIFT_TIME=((CLASS2<<16) + (TYPE_UNS32<<24) + 546)

    #Kinetics Window Size 
    PARAM_KIN_WIN_SIZE=((CLASS2<<16) + (TYPE_UNS16<<24)  + 126)
    
    #General parameters 
    #Is the controller on and running? 
    PARAM_CONTROLLER_ALIVE= ((CLASS2<<16) + (TYPE_BOOLEAN<<24) + 168)
    #Readout time of current ROI, in ms 
    PARAM_READOUT_TIME=((CLASS2<<16) + (TYPE_FLT64<<24) + 179)

    #CAMERA PARAMETERS (CLASS 2)

    PARAM_CLEAR_CYCLES= ((CLASS2<<16) + (TYPE_UNS16<<24) + 97)
    PARAM_CLEAR_MODE=((CLASS2<<16) + (TYPE_ENUM<<24) + 523)
    PARAM_FRAME_CAPABLE=((CLASS2<<16) + (TYPE_BOOLEAN<<24) + 509)
    PARAM_PMODE=((CLASS2<<16) + (TYPE_ENUM <<24) + 524)
    PARAM_CCS_STATUS=((CLASS2<<16) + (TYPE_INT16<<24) + 510)

    #This is the actual temperature of the detector. This is only a get, not a 
    #set                                                                        
    PARAM_TEMP= ((CLASS2<<16) + (TYPE_INT16<<24) + 525)
    #This is the desired temperature to set. 
    PARAM_TEMP_SETPOINT= ((CLASS2<<16) + (TYPE_INT16<<24) + 526)
    PARAM_CAM_FW_VERSION=((CLASS2<<16) + (TYPE_UNS16<<24) + 532)
    PARAM_HEAD_SER_NUM_ALPHA=((CLASS2<<16) + (TYPE_CHAR_PTR<<24)  + 533)
    PARAM_PCI_FW_VERSION=((CLASS2<<16) + (TYPE_UNS16<<24) + 534)
    PARAM_CAM_FW_FULL_VERSION =((CLASS2<<16) + (TYPE_CHAR_PTR<<24)  + 534)

    #Exsposure mode, timed strobed etc, etc 
    PARAM_EXPOSURE_MODE=((CLASS2<<16) + (TYPE_ENUM<<24) + 535)

    #SPEED TABLE PARAMETERS (CLASS 2) 

    PARAM_BIT_DEPTH = ((CLASS2<<16) + (TYPE_INT16<<24) + 511)
    PARAM_GAIN_INDEX = ((CLASS2<<16) + (TYPE_INT16<<24) + 512)
    PARAM_SPDTAB_INDEX = ((CLASS2<<16) + (TYPE_INT16<<24) + 513)
    #define which port (amplifier on shift register) to use. 
    PARAM_READOUT_PORT = ((CLASS2<<16) + (TYPE_ENUM<<24) + 247)
    PARAM_PIX_TIME =((CLASS2<<16) + (TYPE_UNS16<<24) + 516)

    #SHUTTER PARAMETERS (CLASS 2)

    PARAM_SHTR_CLOSE_DELAY= ((CLASS2<<16) + (TYPE_UNS16<<24) + 519)
    PARAM_SHTR_OPEN_DELAY= ((CLASS2<<16) + (TYPE_UNS16<<24) + 520)
    PARAM_SHTR_OPEN_MODE= ((CLASS2<<16) + (TYPE_ENUM <<24) + 521)
    PARAM_SHTR_STATUS= ((CLASS2<<16) + (TYPE_ENUM <<24) + 522)
    PARAM_SHTR_CLOSE_DELAY_UNIT= ((CLASS2<<16) + (TYPE_ENUM <<24) + 543)  #/* use enum TIME_UNITS to specify the unit */

    #I/O PARAMETERS (CLASS 2) 

    PARAM_IO_ADDR=((CLASS2<<16) + (TYPE_UNS16<<24) + 527)
    PARAM_IO_TYPE=((CLASS2<<16) + (TYPE_ENUM<<24) + 528)
    PARAM_IO_DIRECTION=((CLASS2<<16) + (TYPE_ENUM<<24) + 529)
    PARAM_IO_STATE=((CLASS2<<16) + (TYPE_FLT64<<24) + 530)
    PARAM_IO_BITDEPTH=((CLASS2<<16) + (TYPE_UNS16<<24) + 531)

    #GAIN MULTIPLIER PARAMETERS (CLASS 2)

    PARAM_GAIN_MULT_FACTOR=((CLASS2<<16) + (TYPE_UNS16<<24) + 537)
    PARAM_GAIN_MULT_ENABLE=((CLASS2<<16) + (TYPE_BOOLEAN<<24) + 541)
    
    #ACQUISITION PARAMETERS (CLASS 3)
    #(next available index for class three = 11) 

    PARAM_EXP_TIME=((CLASS3<<16) + (TYPE_UNS16<<24) + 1)
    PARAM_EXP_RES=((CLASS3<<16) + (TYPE_ENUM<<24) + 2)
    PARAM_EXP_MIN_TIME=((CLASS3<<16) + (TYPE_FLT64<<24) + 3)
    PARAM_EXP_RES_INDEX=((CLASS3<<16) + (TYPE_UNS16<<24) + 4)
    
    #PARAMETERS FOR  BEGIN and END of FRAME Interrupts
    PARAM_BOF_EOF_ENABLE=((CLASS3<<16) + (TYPE_ENUM<<24) + 5)
    PARAM_BOF_EOF_COUNT=((CLASS3<<16) + (TYPE_UNS32<<24) + 6)
    PARAM_BOF_EOF_CLR=((CLASS3<<16) + (TYPE_BOOLEAN<<24) + 7)


    #Test to see if hardware/software can perform circular buffer 
    PARAM_CIRC_BUFFER=((CLASS3<<16) + (TYPE_BOOLEAN<<24) + 299)

    #Hardware Will Automatically Stop After A Specified Number of Frames
    PARAM_HW_AUTOSTOP=((CLASS3<<16) + (TYPE_INT16<<24) + 166)
    PARAM_HW_AUTOSTOP32=((CLASS3<<16) + (TYPE_INT32<<24) + 166)
    
    #********************** Class 0: Open Camera Modes ***************************
    #Function: pl_cam_open()
    #PI Conversion: CreateController()
    OPEN_MODE={"OPEN_EXCLUSIVE":0}


    #************************ Class 1: Error message size ************************
    ERROR_MSG_LEN=255      # No error message will be longer than this 

    #*********************** Class 2: Cooling type flags *************************
    #used with the PARAM_COOLING_MODE parameter id.                            
    #PI Conversion: NORMAL_COOL = TE_COOLED
    #             CRYO_COOL   = LN_COOLED
    COOLING_TYPE= {"NORMAL_COOL":0, "CRYO_COOL":1} 

    #PARAM_HEAD_COOLING_CTRL
    HEAD_COOLING_TYPE= { "HEAD_COOLING_CTRL_NA" :0, #Not Available
                         "HEAD_COOLING_CTRL_ON":1,  #Turn ON the head cooling
                         "HEAD_COOLING_CTRL_OFF":2} #Turn OFF the head cooling
                         
    #PARAM_COOLING_FAN_CTRL 
    COOLING_FAN_TYPE= {"COOLING_FAN_CTRL_NA":0, #Not Available
                       "COOLING_FAN_CTRL_ON":1, #Enable the cooling fan
                       "COOLING_FAN_CTRL_OFF":2 } #Disable the cooling fan 
                       
    #************************* Class 2: Name/ID sizes ************************
    CCD_NAME_LEN=17           #Includes space for the null terminator 
    MAX_ALPHA_SER_NUM_LEN=32  #Includes space for the null terminator

    #********************** Class 2: MPP capability flags *********************
    #used with the PARAM_MPP_CAPABLE parameter id.                             
    MPP_TYPE={"MPP_UNKNOWN":0, "MPP_ALWAYS_OFF":1, "MPP_ALWAYS_ON":2, "MPP_SELECTABLE":3}

    #********************* Class 2: Shutter flags ***************************
    #used with the PARAM_SHTR_STATUS parameter id.                            
    #PI Conversion: n/a   (returns SHTR_OPEN)
    SHTR_STATUS={"SHTR_FAULT":0, "SHTR_OPENING":1, "SHTR_OPEN":2, "SHTR_CLOSING":3, "SHTR_CLOSED":4,
                 "SHTR_UNKNOWN":5 }

    #************************ Class 2: Pmode constants **************************
    #used with the PARAM_PMODE parameter id.                                   
    PMODE_TYPE= {"PMODE_NORMAL":0, "PMODE_FT":1, "PMODE_MPP":2, "PMODE_FT_MPP":3, "PMODE_ALT_NORMAL":4, 
                 "PMODE_ALT_FT":5, "PMODE_ALT_MPP":6, "PMODE_ALT_FT_MPP":7, "PMODE_INTERLINE":8, "PMODE_KINETICS":9,
                 "PMODE_DIF":10, "PMODE_SPECTRA_KINETICS":11 }

    #************************ Class 2: Color support constants *******************
    #used with the PARAM_COLOR_MODE parameter id.                              
    COLOR_MODE= {"COLOR_NONE":0, "COLOR_RGGB": 2 }

    #************************ Class 2: Attribute IDs *****************************
    #Function: pl_get_param()
    PARAM_ATTR= { 
                  "ATTR_CURRENT":0, 
                  "ATTR_COUNT":1, 
                  "ATTR_TYPE":2, 
                  "ATTR_MIN":3, 
                  "ATTR_MAX":4, 
                  "ATTR_DEFAULT":5,
                  "ATTR_INCREMENT":6, 
                  "ATTR_ACCESS":7, 
                  "ATTR_AVAIL":8
                }

    #************************ Class 2: Access types ******************************/
    #Function: pl_get_param( ATTR_ACCESS )
    ATTR_ACCESS={ 
                  "ACC_ERROR":0, 
                  "ACC_READ_ONLY":1, 
                  "ACC_READ_WRITE":2, 
                  "ACC_EXIST_CHECK_ONLY":3,
                  "ACC_WRITE_ONLY":4
                }
    #This enum is used by the access Attribute */

    #************************ Class 2: I/O types *********************************/
    #used with the PARAM_IO_TYPE parameter id.                                 */
    IO_TYPE={"IO_TYPE_TTL":0, "IO_TYPE_DAC":1}

    #************************ Class 2: Exposure mode flags ***********************/
    """
    used with the PARAM_EXPOSURE_MODE parameter id.                            
    Functions: pl_exp_setup_cont()
             pl_exp_setup_seq()

    PI Conversion:

         Readout Mode: Normal           ROM_KINETICS              ROM_DIF
         ---------------------------------------------------------------------------
            BULB_MODE: (not supported)                            CTRL_EEC
           FLASH_MODE: (Don't have this)
         STROBED_MODE: CTRL_EXTSYNC     CTRL_KINETICS_MULTIPLE    CTRL_ESABI
           TIMED_MODE: CTRL_FREERUN     CTRL_KINETICS_NO_TRIGGER
   TRIGGER_FIRST_MODE: (not supported)  CTRL_KINETICS_SINGLE      CTRL_IEC
  VARIABLE_TIMED_MODE: (Do not use)
      INT_STROBE_MODE: CTRL_INTERNAL_SYNC

    """
    EXPOSURE_MODE= {"TIMED_MODE":0,"STROBED_MODE":1,"BULB_MODE":2, "TRIGGER_FIRST_MODE":3, "FLASH_MODE":4,
                    "VARIABLE_TIMED_MODE":5, "INT_STROBE_MODE":6 }


    #************************ Class 3: Continuous Mode constants *****************/
    #Function: pl_exp_setup_cont()

    CIRC_TYPE= { "CIRC_NONE":0,"CIRC_OVERWRITE":1,"CIRC_NO_OVERWRITE":2}
    
    #/********************** Class 3: Readout status flags ************************/
    """
    Function: pl_exp_check_status()
    PI Conversion: PICM_LockCurrentFrame()
                 PICM_Chk_Data()

    if NEWDATARDY or NEWDATAFIXED     READOUT_COMPLETE
    else if RUNNING                   ACQUISITION_IN_PROGRESS
    else if INITIALIZED or DONEDCOK   READOUT_NOT_ACTIVE
    else                              READOUT_FAILED

    """
    CHECK_STATUS= { 
                  "READOUT_NOT_ACTIVE":0, 
                  "EXPOSURE_IN_PROGRESS":1, 
                  "READOUT_IN_PROGRESS":2,
                  "READOUT_COMPLETE":3,                   #Means frame available for a circular buffer acq
                  "FRAME_AVAILABLE" : 4, #New camera status indicating at least one frame is available
                  "READOUT_FAILED":5, 
                  "ACQUISITION_IN_PROGRESS":6,
                  "MAX_CAMERA_STATUS":7
                  }
    
    #********************** Class 3: Abort Exposure flags ************************/
    #Function: pl_exp_abort()
    #PI Conversion: controller->Stop(), enum spec ignored

    CCS = { "CCS_NO_CHANGE":0, "CCS_HALT":1, "CCS_HALT_CLOSE_SHTR":2, "CCS_CLEAR":3,
            "CCS_CLEAR_CLOSE_SHTR":4, "CCS_OPEN_SHTR":5, "CCS_CLEAR_OPEN_SHTR":6}

    
    def __init__(self, serialNumber=None, connect=True,debug=False):
      """  Loads DLL"""

      if debug==False:
         from ctypes import windll
         #self.lib = windll.LoadLibrary(self.LIBNAME + ".dll")

      #super(PrincetonInstrumentCCD, self).__init__(serialNumber, connect,debug)
      #print("Try bitshift")
      #print(bin((self.TYPE_DICT["TYPE_INT16"]<<24) +(1)))
      #print(bin((self.TYPE_DICT["TYPE_INT16"]<<24) +(2)))
      #print((self.TYPE_DICT["TYPE_INT16"]<<24) +2)
   
def main():
    
    app = QtGui.QApplication(sys.argv)
    ex = PrincetonInstrumentCCD()
    #print(ex.PARAM_DICT["PARAM_DD_VERSION"])
    #print(ex.PARAM_DICT["PARAM_DD_RETRIES"])
    #print(ex.CLASSES_DICT["CLASS99"])
    #print(ex.IO_TYPE(0))

    print(ex.IO_TYPE["IO_TYPE_DAC"])
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
