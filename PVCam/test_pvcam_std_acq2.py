# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 13:26:36 2015

@author: Helene
"""
from __future__ import division
from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals
import time
import math
# to load the proper dll
import platform
import PrincetonInstrument as PI
import matplotlib.pyplot as plt
import numpy as np
import matplotlib
# Do not import or use ill definied data types
# such as short int or long
# use the values specified in the h file
# float is always defined as 32 bits
# double is defined as 64 bits
from ctypes import byref, POINTER, create_string_buffer, c_float, \
    c_int16, c_int32, c_uint32, c_void_p, CFUNCTYPE, c_char_p, c_char, c_bool, addressof, pointer, sizeof, CDLL, c_ushort, c_double
from ctypes import c_int32 as c_enum
from ctypes import windll
from ctypes import cast
from ctypes import Structure


class rgn_type(Structure):
    pass
rgn_type._fields_ = [
        ('s1', c_ushort),  #First pixel in the serial register
        ('s2', c_ushort), #Last pixel in the serial register
        ('sbin', c_ushort), #Serial binning for this region 
        ('p1', c_ushort), #First row in the parallel register
        ('p2', c_ushort), #Last row in the parallel register 
        ('pbin', c_ushort)] #Parallel binning for this region 
rng_ptr=POINTER(rgn_type)
rgn_const_ptr= POINTER(rgn_type)
if __name__ == "__main__":
    print(__doc__)
    
    Camera=PI.PrincetonInstrumentCCD()
    LIBNAME="Pvcam32"
    lib = windll.LoadLibrary("C:\Program Files\Princeton Instruments\Picam\Runtime\Picam.dll")
    print(lib)
    cam_name=create_string_buffer(12)
      
    total_cams=10
    handle=c_int16()
    version=1
    c_total_cams=c_int16(total_cams)
    print("Initializes camera:")
    #print(c_total_cams)
    print(lib.pl_pvcam_init())
    print("pl_pvcam_init error code:")
    print(lib.pl_error_code())
    #print(lib.pl_cam_get_name(c_int16(0), c_char_p(cam_name) ))
    print("number of cameras before")    
    print(c_total_cams)    
    print(lib.pl_cam_get_total(byref(c_total_cams)))
    print("number of cameras after")    
    print(c_total_cams)
    print("pl_cam_get_total error code:")
    print(lib.pl_error_code())
    
    print("Try pl_cam_get_name:")
    print(lib.pl_cam_get_name(c_int16(0),cam_name))
    print(cam_name.value)
    print("pl_cam_get_name error code:")
    print(lib.pl_error_code())
    
    print("Is this the handle of an open camera?")
    print(lib.pl_cam_check(handle))
    print("Try pl_cam_open:")
    print(lib.pl_cam_open(cam_name,byref(handle), c_int16(0)))
    print("pl_cam_open error code:")
    print(lib.pl_error_code())
    print("And now is this the handle of an open camera?")
    print(lib.pl_cam_check(handle))
    #print("pl_cam_open error code:")
    #print(lib.pl_error_code())

    #Checks for circular buffer support
    attr_avail=True
    print("checks for circular buffer support")
    function_flag=(bool(lib.pl_get_param(handle,c_uint32(Camera.PARAM_CIRC_BUFFER), c_int16(Camera.PARAM_ATTR["ATTR_AVAIL"]), byref(c_bool(attr_avail))) ))   
    print(attr_avail)
    print("are we going there?")

    
    #print("are we going there?")
    #plt.show() 
    
    if function_flag == True and attr_avail == True:
        
        region=rgn_type(1,1300,1,1,10,10)
        no_regions=1
        region_array= (rgn_type *no_regions)(region)
        print(region.s1, region.s2, region.p1)
        size=c_uint32()
        #frame=pointer(c_int16())
        status=c_int16()
        not_needed=c_uint32(1)
        numberframes=5
        print("size before:")
        print(size)
        print("try pl_exp_init:")
        print(lib.pl_exp_init_seq())
        print("pl_exp_init_seq error message:")
        print(lib.pl_error_code())
        time.sleep(1)
        exp_mode=c_double()
        PARAM_READOUT_TIME=67240115
        ATTR_CURRENT=0
        print("try pl_get_param")
        print(lib.pl_get_param(handle,c_uint32(PARAM_READOUT_TIME), c_int16(ATTR_CURRENT), byref(exp_mode)))
        print("pl_get_param error code:")
        print(lib.pl_error_code())
        print("Readout_time before:")        
        print(exp_mode)
        print("try pl_exp_setup_seq:")
        print(lib.pl_exp_setup_seq(handle,1, 1,byref(region), 0,c_uint32(1), byref(size)))
        print("pl_exp_setup_seq error message:")
        print(lib.pl_error_code())
        time.sleep(1)
     
        print("try pl_cam_get_diags")
        print(lib.pl_cam_get_diags(handle))
        print("pl_cam_get_diags error code:")
        print(lib.pl_error_code())
        
        print(lib.pl_get_param(handle,c_uint32(PARAM_READOUT_TIME), c_int16(ATTR_CURRENT), byref(exp_mode)))
        print("pl_get_param error message:")
        print(lib.pl_error_code())
        print("size")        
        print(size.value)
        print("region parameters")
        print(region.s1, region.s2,region.p1)
        print("Readout_time after:")        
        print(exp_mode)
        
        #mylib=CDLL("msvcrt")
        #mylib.malloc.restype=c_void_p
        #data_buffer=mylib.malloc(44 * sizeof(c_int16))
        #data_buffer=cast(data_buffer, POINTER(c_int16))
        #data_buffer=POINTER(c_int16)(c_int16(3))
        #frame=(c_int16 * 4244)(np.ones(4244))
        data_buffer=(c_int16 *(size.value//2))()
        for i in range(len(data_buffer)):
            print(data_buffer[i])
        #data_buffer=(np.empty(size.value,dtype=np.int16))  
        #print("Frame values:")
        print(data_buffer[0], data_buffer[1], data_buffer[2], data_buffer[3], data_buffer[-1])#data_buffer_Ptr=cast(data_buffer, c_void_p)
        #data_buffer_Ptr = data_buffer.ctypes.data_as(POINTER(c_int16))
        #print("Frame values:")
        #print(data_buffer[3], data_buffer[4], data_buffer[5], data_buffer[6], data_buffer[0], data_buffer[1], data_buffer[99])
        #frame=lib.malloc(size * sizeof(c_int16))
        #frame=cast(frame, POINTER(c_int16))
        #frame=(np.empty(4, dtype=int))  
        #frame_ptr=frame.ctypes.data_as(POINTER(c_int16))
        print("Status here:")
        print(status)
        print("Try pl_exp_start_seq")
        print(lib.pl_exp_start_seq(handle,data_buffer))
        print("pl_exp_start_seq error message:")
        print(lib.pl_error_code())
        status_flag= lib.pl_exp_check_status(handle, byref(status), byref(not_needed))
        while status.value == 1 or status.value==2 or status.value ==5:
            status_flag= lib.pl_exp_check_status(handle, byref(status), byref(not_needed))
            print(lib.pl_error_code())
            print(status_flag)
            print(status)
            print("Byte counts")
            print(not_needed)
        print("THIS SEEMS TO HAVE WORKED!!")
        print(status.value)
        #print("Frame values:")
        #for i in range(len(data_buffer)):
        #    print(data_buffer[i])
        
     
        """
        while numberframes > 0:   
            print("Status here:")
            print(status)
            print("Try pl_exp_start_seq")
            print(lib.pl_exp_start_seq(c_int16(handle),frame))
            print("pl_exp_start_seq error message:")
            print(lib.pl_error_code())
            #print("frame")
            #print(frame_ptr)
            lib.pl_get_param(c_int16(handle),c_uint32(Camera.PARAM_READOUT_TIME), c_int16(Camera.PARAM_ATTR["ATTR_CURRENT"]), byref(exp_mode))  
            print("Readout time")
            print(exp_mode)            
            status_flag= bool(lib.pl_exp_check_status(c_int16(handle), byref(status), byref(not_needed)))
            #print(status_flag)
            #print(status)
            time.sleep(1)
            while status_flag==True and (status.value != Camera.CHECK_STATUS["FRAME_AVAILABLE"] and status.value != Camera.CHECK_STATUS["READOUT_FAILED"]) :
                  time.sleep(1)
                  lib.pl_get_param(c_int16(handle),c_uint32(Camera.PARAM_READOUT_TIME), c_int16(Camera.PARAM_ATTR["ATTR_CURRENT"]), byref(exp_mode))  
                  print("Readout time")
                  print(exp_mode)
                  
                  status_flag= bool(lib.pl_exp_check_status(c_int16(handle), byref(status), byref(not_needed)))
                  print(status)
                  if status.value == Camera.CHECK_STATUS["READOUT_FAILED"]:
                      print("Data collection error: %i\n", lib.pl_error_code() )
            
            if status.value==4:
                print("This is a frame:")
                print(frame[0].contents, frame[1].contents, frame[2].contents)      
            numberframes=numberframes -1
            print( "Remaining Frames %i\n", numberframes )
        """
        """
        frame=
        data_buffer=(np.empty(buffer_size, dtype=np.int16))   
        data_buffer_Ptr = data_buffer.ctypes.data_as(POINTER(c_int16))
        print( "Collecting %i Frames\n", numberframes )
        lib.pl_exp_start_cont(c_int16(handle), data_buffer_Ptr, c_uint32(buffer_size));
        print("pl_exp_start_cont error message:")
        print(lib.pl_error_code())
       
        while numberframes > 0:
  
          status_flag= bool(lib.pl_exp_check_cont_status(c_int16(handle), byref(status), byref(not_needed), byref(not_needed)))
  
          #while status_flag==True and status != Camera.CHECK_STATUS["READOUT_COMPLETE"] and status != Camera.CHECK_STATUS["READOUT_FAILED"] :
          print("Get latest frame status")
          print(lib.pl_exp_get_latest_frame( c_int16(handle), byref(adress)))
          #   if status != Camera.CHECK_STATUS["READOUT_FAILED"]:
          #       print("Data collection error: %i\n", lib.pl_error_code() )

             #if bool(lib.pl_exp_get_latest_frame( c_int16(handle), adress )) == True: 
                 # address now points to valid data */
          #       print( "Center Three Points: %i, %i, %i\n")
               #*((uns16*)address + frame_size/sizeof(uns16)/2 - 1),
               #*((uns16*)address + frame_size/sizeof(uns16)/2),
               #*((uns16*)address + frame_size/sizeof(uns16)/2 + 1) );
          numberframes=numberframes -1
          print( "Remaining Frames %i\n", numberframes )
        """
        #Stop the acquisition */
        
        print("pl_exp_stop_cont error message:")
        print(lib.pl_error_code())
        #Finish the sequence */
        print("try pl_exp_finish_seq:")
        print(lib.pl_exp_finish_seq(handle, data_buffer,0))
        print("pl_exp_finish_seq error message:")
        print(lib.pl_error_code())
        #Uninit the sequence */
        print("try pl_exp_uninit_seq:")
        print(lib.pl_exp_uninit_seq())
        print("pl_exp_uninit error message:")
        print(lib.pl_error_code())
        #free( buffer );
    else:
        print("Circular Buffer mode not supported")
    
   
    print("try pl_cam_get_diags")
    print(lib.pl_cam_get_diags(handle))
    print("pl_cam_get_diags error code:")
    print(lib.pl_error_code())
    
    attr_avail=c_bool(0)
    print("Attr available before:")    
    print(attr_avail)
    print("try pl_get_param with ATTR_AVAIL")
    print(lib.pl_get_param(handle,c_uint32(Camera.PARAM_READOUT_TIME), c_int16(Camera.PARAM_ATTR["ATTR_AVAIL"]), byref(attr_avail)))    
    print("pl_cam_get_param error code:")
    print(lib.pl_error_code())
    print("Attr available after:")
    print(attr_avail)    
    
    attr_access=c_int16(0)
    print("Attr access before:")    
    print(attr_access)
    print("try pl_get_param with ATTR_ACCESS")
    print(lib.pl_get_param(handle,c_uint32(Camera.PARAM_READOUT_TIME), c_int16(Camera.PARAM_ATTR["ATTR_ACCESS"]), byref(attr_access))) 
    print("pl_cam_get_param error code:")
    print(lib.pl_error_code())    
    print("Attr access after:")    
    print(attr_access)
    
    attr_type=c_int16(0)
    print("Attr type before:")    
    print(attr_type)
    print("try pl_get_param with ATTR_TYPE")
    print(lib.pl_get_param(handle,c_uint32(Camera.PARAM_READOUT_TIME), c_int16(Camera.PARAM_ATTR["ATTR_TYPE"]), byref(attr_type))) 
    print("pl_cam_get_param error code:")
    print(lib.pl_error_code())    
    print("Attr type after:")    
    print(attr_type)
    
    attr_current=c_double(0)
    print("Attr current before:")    
    print(attr_current)
    print("try pl_get_param with ATTR_CURRENT")
    print(lib.pl_get_param(handle,c_uint32(Camera.PARAM_READOUT_TIME), c_int16(Camera.PARAM_ATTR["ATTR_CURRENT"]), byref(attr_current))) 
    print("pl_cam_get_param error code:")
    print(lib.pl_error_code())    
    print("Attr current after:")    
    print(attr_current)
    print("try pl_set_param with ATTR_CURRENT")
    print(lib.pl_get_param(handle,c_uint32(Camera.PARAM_READOUT_TIME), c_int16(Camera.PARAM_ATTR["ATTR_CURRENT"]), byref(attr_current))) 
    print("pl_cam_get_param error code:")
    print(lib.pl_error_code())    
    print("Attr current after:")    
    
    
    
    print("try pl_cam_close:")
    print(lib.pl_cam_close(handle))
    print("pl_cam_close error code:")
    print(lib.pl_error_code())
    
    print(lib.pl_pvcam_uninit())
    
    #fig= plt.figure()
    #x_data= np.arange(0,size.value//2,1)
    #print(len(x_data))
    #print(len(data_buffer))
    dat = np.fromiter(data_buffer, np.uint16)
    plt.plot(dat)
    plt.show()
    #plt.plot([1,2,3,4])
    #plt.savefig('test.png')
    print("Done")
    