# -*- coding: utf-8 -*-
"""
Created on Thu Mar 19 13:26:36 2015

@author: Helene
"""
from __future__ import division
from __future__ import absolute_import
from __future__ import print_function
from __future__ import unicode_literals
import time
# to load the proper dll
import numpy as np
from ctypes import *

#defines rgn_type exactly the same way you did in pvcam_h.py
class rgn_type(Structure):
    pass
rgn_type._fields_ = [
        ('s1', c_ushort),  #First pixel in the serial register
        ('s2', c_ushort), #Last pixel in the serial register
        ('sbin', c_ushort), #Serial binning for this region 
        ('p1', c_ushort), #First row in the parallel register
        ('p2', c_ushort), #Last row in the parallel register 
        ('pbin', c_ushort)] #Parallel binning for this region 
rng_ptr=POINTER(rgn_type)
rgn_const_ptr= POINTER(rgn_type)

if __name__ == "__main__":
    print(__doc__)
    
    lib = windll.LoadLibrary("Pvcam32.dll")
    
    total_cams=c_int16()
    cam_name=create_string_buffer(12)
    handle=c_int16()
    
    #initializes the camera
    print(lib.pl_pvcam_init()) #interpreter ouptut: 1
    print(lib.pl_error_code()) #interpreter ouptut: 0
    
    print(lib.pl_cam_get_total(byref(total_cams))) #interpreter ouptut: 1
    print(lib.pl_error_code()) #interpreter ouptut: 0
    
    print(lib.pl_cam_get_name(c_int16(0),cam_name)) #interpreter ouptut: 1
    print(cam_name.value) #interpreter ouptut: camera1
    print(lib.pl_error_code()) #interpreter ouptut: 0

    #opens camera
    OPEN_EXCLUSIVE=0
    print(lib.pl_cam_open(cam_name,byref(handle), c_int16(OPEN_EXCLUSIVE))) #interpreter ouptut: 1
    print(lib.pl_error_code()) #interpreter ouptut: 0
    #checks that it is the handle of an open camera
    print(lib.pl_cam_check(handle)) #interpreter ouptut: 1
        
    print(lib.pl_exp_init_seq()) #interpreter ouptut: 1
    print(lib.pl_error_code()) #interpreter ouptut: 0

    readout_time=c_float()
    PARAM_READOUT_TIME=67240115 #I verified that this is the right number in my pvcam.h
    ATTR_CURRENT=0 #same, this is the first element in the enum 
    
    #gets the readout time here
    print(lib.pl_get_param(handle,c_uint32(PARAM_READOUT_TIME), c_int16(ATTR_CURRENT), byref(readout_time))) #interpreter ouptut: 1065091073
    print(lib.pl_error_code()) #interpreter ouptut: 0   
    print(readout_time.value) #interpreter output: 0.9880000352859497 (Note: close to what is obtained in Winspec, where we get 0.92ms.
    #Even though current settings might be slightly different from Winspec ones, the range of the value seems correct)
    
    #tries to setup a sequence
    #creates a region. Our CCD array has 1340 sp and 20 pp. We do full binning in the p direction. Here I just select a sub-region.
    region=rgn_type(1,100,1,1,10,9) #to do full binning it should actually be pbin=10 as (10 - 1 + 1)/pbin
    blength=c_uint32()
    #C function: rs_bool pl_exp_setup_seq(int16 hcam,uns16 exp_total,uns16 rgn_total,rgn_const_ptr rgn_array,int16 mode,uns32 exposure_time,uns32_ptr stream_size)
    TIMED_MODE=0
    print(lib.pl_exp_setup_seq(handle,1, 1,byref(region), c_int16(TIMED_MODE),c_uint32(100), byref(blength))) #interpreter ouptut: 1
    print(lib.pl_error_code()) #interpreter ouptut: 0
    #prints blength value
    print(blength.value) #Interpreter output: 200 (why not 100?)
   
    print(lib.pl_cam_get_diags(handle)) #interpreter ouptut: 1
    print(lib.pl_error_code()) #interpreter ouptut: 0
     
    #now checks again the readout time
    print(lib.pl_get_param(handle,c_uint32(PARAM_READOUT_TIME), c_int16(ATTR_CURRENT), byref(readout_time))) #interpreter ouptut: 391643137
    print(lib.pl_error_code()) #interpreter ouptut: 0
    print(readout_time.value) #interpreter ouptut: 7.007872968624475e-25 (Note: now the readout_time has a crazy value!)
        
    #creates a data buffer, like you do
    data_buffer=(c_int16 * (blength.value//2))()

    #tries to start a sequence    
    print(lib.pl_exp_start_seq(handle,data_buffer)) #interpreter ouptut: 1
    print(lib.pl_error_code()) #interpreter ouptut: 0
    
    #here prints data_buffer values. Only the first two and the last two values are modified.
    #data_buffer[0]=-12578, data_buffer[1]= -21267, data_buffer[-2]= -8531, data_buffer[-1]= -1314 and data_buffer[i]=0 for the rest
    for i in range(len(data_buffer)):
            print(data_buffer[i])
            
    #performs a status check   
    status=c_int16()
    not_needed=c_uint32()
 
    print(lib.pl_exp_check_status(handle, byref(status), byref(not_needed))) #interpreter ouptut: 1
    print(lib.pl_error_code()) #interpreter ouptut: 0
    print(status.value) #interpreter output: 5 (Note: in my pvcam.h, an index of 5 corresponds to 
    # READOUT_FAILED, as can be seen right below)
    
    """    
    enum
        { 
          READOUT_NOT_ACTIVE, 
          EXPOSURE_IN_PROGRESS, 
          READOUT_IN_PROGRESS,
          READOUT_COMPLETE,                   /* Means frame available for a circular buffer acq */
          FRAME_AVAILABLE = READOUT_COMPLETE, /* New camera status indicating at least one frame is available */
          READOUT_FAILED, 
          ACQUISITION_IN_PROGRESS,
          MAX_CAMERA_STATUS
        };
    """
    print("Byte counts")
    print(not_needed.value) #interpreter ouptut: 0
    
    #finishes the sequence
    print(lib.pl_exp_finish_seq(handle, data_buffer,0)) #interpreter ouptut: 1
    print(lib.pl_error_code()) #interpreter ouptut: 0
    
    #uninitializes the sequence
    print(lib.pl_exp_uninit_seq()) #interpreter ouptut: 1
    print(lib.pl_error_code()) #interpreter ouptut: 0
    
    #closes the camera
    print(lib.pl_cam_close(handle)) #interpreter ouptut: 1
    print(lib.pl_error_code()) #interpreter ouptut: 0
    
    #uninitializes the camera
    print(lib.pl_pvcam_uninit()) #interpreter ouptut: 1

   