# -*- coding: utf-8 -*-
"""
Created on Wed Jun 17 13:58:32 2015

@author: Helene Seiler
Copyright © 2015 Helene Seiler, McGill University

This is a minimal GUI to 

- plot a PIXIS spectrum in real time: to align laser beam in CCD
- plot latest acquired FIFO packet
- set CCD I/O : trigger + OUT signal
- set exposure time
- set data format: enable/disable frame tracking, enable/disable timestamp
- display acquisition info (readout time, etc..)

Inspired from BasicSpectroSoftware.py, written by same author
"""

from __future__ import division
import picam
from PyQt4 import QtGui, QtCore
from PyQt4.QtCore import QReadWriteLock
import sys
import pyqt_window
import numpy as np
import time
import ctypes

class PIXIS_Widget(QtGui.QWidget):
      """
      """
      def __init__(self, parent=None):

        super(PIXIS_Widget, self).__init__(parent)
        
        #Creates a picam instance
        self.PIXIS=picam.PICam()

        #SETS ALL PIXIS PARAMETERS NEEDED TO PERFORM 2D EXPERIMENT
        #Defines ROI. For spectroscopy, we only need a single ROI. We also perform full
        #vertical binning.
        self.PIXIS_info= self.PIXIS.get_all_info()
        
        self.PIXIS_ROI_width= self.PIXIS_info['Sensor Info']['Sensor Active Width']     
        self.PIXIS_ROI_height= self.PIXIS_info['Sensor Info']['Sensor Active Height']
        self.PIXIS_vertical_binning= self.PIXIS_ROI_height
        self.PIXIS.add_ROI(0,self.PIXIS_ROI_width,1,0,self.PIXIS_ROI_height, self.PIXIS_vertical_binning)
        self.wavelengths_acton = np.arange(0, self.PIXIS_ROI_width, 1)
        
        #Sets camera timeout in ms
        self.PIXIS_timeout=2000
        
        self.lock = QReadWriteLock()
        self.PIXIS.setup_acquire_cont()
        self.PIXIS_thread = PIXIS_thread(self.lock, self.PIXIS_timeout, self)
        #Sets exposure and shutter parameters
        #PicamParameter_ExposureTime =0 (we want minimum exposure time for our application)
        #PicamParameter_ShutterClosingDelay =0 (we want to acquire as fast as possible so no extra delay!! Also, laser pulses vs stray light in dark room..)
        #PicamParameter_ShutterTimeResoultion=1 (us)
        #PicamParameter_ShutterTimingMode=  PicamShutterTimingMode_normal (1)
        #self.PIXIS.set_shutter_timing(0, 0,2, 1)
        self.PIXIS.set_exposure_time(0)
        
        #Sets Adc settings
        #AdcAnalogGain: 1 (PicamAdcAnalogGain_Low (allows for digitization of larger signals))
        #AdcBitDepth: 12
        #AdcQuality: 1 (PicamAdcQuality_LowNoise)
        #AdcSpeed: 2 (in MHz, fastest possible)
        self.PIXIS.set_Adc_params(1, 16, 1, 2)
        
        #Sets I/O settings
        #response: 1 (PicamTriggerResponse_NoResponse)
        #determination: 1 (PicamTriggerDetermination_PositivePolarity)
        #signalOUT: 1 (PicamOutputSignal_NotReadingOut)
        self.PIXIS.set_IO_settings(1,1,1)
        
        #sets readout controls
        #verticalshiftrate:  9.2  # 3.2 to 30.2 us, in 3 us increments
        #( Note:  The less time there is to shift the charge, the less efficient the shift becomes.
        #The default will shift all the charge, while shifting faster will lose some charge.
        #3.2 us will shift 0 charge in some systems.  15.2, or 9.2 us is fast enough without losing too much charge. )
        #readoutcontroldmode: 1 (PicamReadoutControlMode_FullFrame)
        #kineticswindowheight:
        
        self.PIXIS.set_readout_controls(9.2, readoutcontrolmode= 1)

        #sets sensor cleaning
        
        self.PIXIS.commit_params()
        
        #Creates a QTimer used in the continuous plotting mode for updating the plot
        self.refresh = 50
        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.plot_spectrum) 
        #USBself.timer.stop()
        
        #DisplayParamWidget to display PIXIS parameters
        #Everything that has to do with the Menu
        self.myQMenuBar = QtGui.QMenuBar(self)
        exitMenu = self.myQMenuBar.addMenu('File')
        exitAction = QtGui.QAction('Exit', self)
        exitAction.triggered.connect(QtGui.qApp.quit)
        exitMenu.addAction(exitAction)
        
        settingsMenu= self.myQMenuBar.addMenu('Settings')
        save_data= QtGui.QAction('Data saving options', self)
        save_data.triggered.connect(self.update_save_data_options)
        settingsMenu.addAction(save_data)
        
        display_all_settings= QtGui.QAction('Display all CCD settings', self)
        display_all_settings.triggered.connect(self.display_all_CCD_settings)
        settingsMenu.addAction(display_all_settings)
        #PyQtGrapHWidget is a class defined in pyqt_window.py which can be used
        #for plotting any data using the pyqtgraph package. It inherits from
        # pyqtgraph.PlotWidget
        self.win = pyqt_window.PyQtGraphWidget(n_curves=4)
        #Here need to replace the 2000 with variables. 
        self.win.setYRange(0, 2000)
        self.win.setTitle("Spectrum")
        self.win.setLabel('left', 'Intensity', units='a.u')
        self.win.setLabel('bottom', 'Pixel', units='')

        #Creates cursors on the plot for setting the fitting boundaries
        self.vlines = []
        self.vlines.append(self.win.add_datacursor("x", True))
        self.vlines.append(self.win.add_datacursor("x", True))

        #Creates SpinBoxes to display cursor values.    
        self.xmin_cursor = QtGui.QDoubleSpinBox()
        self.xmin_cursor.setMinimum(0)
        self.xmin_cursor.setMaximum(self.PIXIS_ROI_width)
        self.xmin_cursor.setStyleSheet('QDoubleSpinBox {color: blue;}')

        self.xmax_cursor = QtGui.QDoubleSpinBox()
        self.xmax_cursor.setMinimum(0)
        self.xmax_cursor.setMaximum(self.PIXIS_ROI_width)
        self.xmax_cursor.setStyleSheet('QDoubleSpinBox {color: rgb(0, 102, 0);}')

        #Cursor x_min properties
        self.vlines[0].sigPositionChanged.connect(self.update_position_xmin_cursor)
        self.vlines[0].setBounds((0, self.PIXIS_ROI_width))
        self.vlines[0].setPen('b', width=3)
        self.vlines[0].setHoverPen(color=QtGui.QColor(0, 128, 255, 255), width=3)

        #Cursor x_max properties
        self.vlines[1].sigPositionChanged.connect(self.update_position_xmax_cursor)
        self.vlines[1].setBounds((0, self.PIXIS_ROI_width))
        self.vlines[1].setPen(color=QtGui.QColor(0, 102, 0, 255), width=3)
        self.vlines[1].setHoverPen(color=QtGui.QColor(102, 255, 102, 255), width=3)
        self.vlines[1].setValue(self.PIXIS_ROI_width)

        self.xmin_cursor.valueChanged.connect(self.update_cursor_xmin_position)
        self.xmax_cursor.valueChanged.connect(self.update_cursor_xmax_position)
        #self.lock = QReadWriteLock()
        #self.spectro_thread = Spectro_thread(self.lock, self.spectro, self.nshots, self)
        
        #Creates button "Acquire"
        self.acquire_button = QtGui.QPushButton(self)
        self.acquire_button.setText("Acquire Spectrum")
        self.acquire_button.setFixedHeight(30)
        self.acquire_button.setFixedWidth(150)
        self.acquire_continuous_tick = QtGui.QCheckBox("Continuous")
        self.acquire_button.clicked.connect(self.start_acquisition)

        #Creates "stop button"
        self.stop_button = QtGui.QPushButton(self)
        self.stop_button.setText("Stop")
        self.stop_button.setFixedHeight(30)
        self.stop_button.setFixedWidth(150)
        self.stop_button.setEnabled(False)
        self.stop_button.clicked.connect(self.stop_acquisition)
        
        
        #Creates "exposure time" settings
        self.exposure_time_label= QtGui.QLabel('Exposure time [us]')
        self.exposure_time_spinbox= QtGui.QSpinBox(self)
        self.exposure_time_spinbox.setMaximum(1000)
        self.exposure_time_spinbox.valueChanged.connect(self.update_exposure_time)
        
        
        #Creates "Combo Box for trigger choice"
        self.trigger_response_combo = QtGui.QComboBox(self)
        #No response to trigger (shutter conditions: normal, see chap. 6 of PIXIS manual)
        self.trigger_response_combo.addItem("Free run")
        #
        self.trigger_response_combo.addItem("External sync")
        self.trigger_response_combo.addItem("External sync with continuous cleans")
        self.trigger_response_label= QtGui.QLabel('Trigger type:')
        self.trigger_response_combo.currentIndexChanged.connect(self.update_IO_settings)
        
        #Creates "Combo Box for signal OUT"
        #The order matters here
        self.signal_out_combo= QtGui.QComboBox(self)
        self.signal_out_combo.addItem("NOT SCAN (not reading out)")
        self.signal_out_combo.addItem("SHUTTER (shutter open)")
        self.signal_out_combo.addItem("BUSY")
        self.signal_out_combo.addItem("Always LOW")
        self.signal_out_combo.addItem("Always HIGH")
        self.signal_out_label = QtGui.QLabel("Logic OUT control")
        self.signal_out_combo.currentIndexChanged.connect(self.update_IO_settings)
        
        #path to wavelengths.txt
        #wavelengths.txt is a file containing the wavelengths for the callibration that was performed
        #for a given run of experiments
        self.QIconLoad= QtGui.QIcon("load.png")
        self.QLabel_path= QtGui.QLabel("Path to wavelengths_acton.txt: ")
        self.QLineEdit_path= QtGui.QLineEdit(' ')
        self.QLineEdit_path.setReadOnly(True)
        self.QLineEdit_path.setMinimumWidth(500)
        self.QPushButton_path= QtGui.QPushButton()
        self.QPushButton_path.setIcon(self.QIconLoad)
        self.QPushButton_path.setMaximumWidth(30)
        self.QPushButton_path.clicked.connect(self.load_path)
        

        #Display Param Widget  
        self.display_info_widget= DisplayParamWidget(self.PIXIS_info, 'General CCD info')      
        self.display_info_widget.show()
        
        #self.display_adc_widget= DisplayParamWidget(self.)
        #LAYOUT STUFF  
        # Grid 1 contains the acquire, background and stop buttons
        self.Grid1 = QtGui.QGridLayout()
        self.Grid1.setColumnStretch(0, 1)
        self.Grid1.addWidget(self.acquire_button, 0, 0, QtCore.Qt.AlignVCenter)
        self.Grid1.addWidget(self.acquire_continuous_tick, 0, 1, QtCore.Qt.AlignVCenter)
        self.Grid1.addWidget(self.stop_button, 1, 0, QtCore.Qt.AlignVCenter)
        self.Grid1.addWidget(self.trigger_response_label, 2, 0, QtCore.Qt.AlignVCenter)
        self.Grid1.addWidget(self.trigger_response_combo, 2, 1, QtCore.Qt.AlignVCenter)
        self.Grid1.addWidget(self.signal_out_label, 3, 0, QtCore.Qt.AlignVCenter)
        self.Grid1.addWidget(self.signal_out_combo, 3, 1, QtCore.Qt.AlignVCenter)
        self.Grid1.addWidget(self.QLabel_path, 4, 0, QtCore.Qt.AlignVCenter)
        self.Grid1.addWidget(self.QLineEdit_path, 4, 1, QtCore.Qt.AlignVCenter)
        self.Grid1.addWidget(self.QPushButton_path, 4, 2, QtCore.Qt.AlignVCenter)
        self.Grid1.addWidget(self.exposure_time_label, 5, 0, QtCore.Qt.AlignVCenter)
        self.Grid1.addWidget(self.exposure_time_spinbox,5,1, QtCore.Qt.AlignVCenter)
        self.Grid1.setRowMinimumHeight(1, self.acquire_button.height())
        self.Grid1.setRowMinimumHeight(2, self.stop_button.height())
        
        #Window properties are set here
        self.setGeometry(500, 500, 450, 650)
        self.setMaximumHeight(1000)
        self.setMaximumWidth(700)
        self.setWindowTitle(self.PIXIS_info['Model'] + ' Interface')
        QtGui.QApplication.setStyle("windowsxp")
        
        #Vlayout is the main layout
        self.Vlayout = QtGui.QVBoxLayout()
        self.Vlayout.addWidget(self.myQMenuBar)
        self.Vlayout.addLayout(self.Grid1)
        self.Vlayout.addWidget(self.win)

        self.setLayout(self.Vlayout)
        self.show()
        
        self.connect(self.PIXIS_thread, QtCore.SIGNAL('spectrum_acquired()'), self.plot_spectrum)
        
        # The four following methods update x_min and x_max cursor values
      def update_position_xmin_cursor(self):
        self.xmin_cursor.setValue(self.vlines[0].value())

      def update_position_xmax_cursor(self):
        self.xmax_cursor.setValue(self.vlines[1].value())

      def update_cursor_xmin_position(self):
        self.vlines[0].setValue(self.xmin_cursor.value())

      def update_cursor_xmax_position(self):
        self.vlines[1].setValue(self.xmax_cursor.value())
        
      def update_cursors_max_boundaries(self):
          #redefines max and min values for x cursors
          self.xmin_cursor.setMinimum(self.wavelengths_acton[0])
          self.xmin_cursor.setMaximum(self.wavelengths_acton[-1])
          self.vlines[0].setBounds((self.wavelengths_acton[0], self.wavelengths_acton[-1]))
          self.xmax_cursor.setMinimum(self.wavelengths_acton[0])
          self.xmax_cursor.setMaximum(self.wavelengths_acton[-1])
          self.vlines[1].setBounds((self.wavelengths_acton[0], self.wavelengths_acton[-1]))
          #redefines win x label
          self.win.setLabel('bottom', 'Wavelength', units='nm')
          
      def update_IO_settings(self):
          """
          This function is called when the user changes the trigger or SIGNAL OUT boxes. It simply updates
          the IO parameters accordingly and commit parameters
          """
          #response: 1 (PicamTriggerResponse_NoResponse)
          #determination: 1 (PicamTriggerDetermination_PositivePolarity)
          #signalOUT: 1 (PicamOutputSignal_NotReadingOut)

          self.PIXIS.set_IO_settings(self.trigger_response_combo.currentIndex() + 1,1,
                                     self.signal_out_combo.currentIndex() +1 )
          self.PIXIS.commit_params()
        
      def update_exposure_time(self):
          """
          This function is called when the user changes the exposure time. It simply updates
          the exposure time and commit parameters
          """
          self.PIXIS.set_exposure_time(self.exposure_time_spinbox.value())
          self.PIXIS.commit_params

          
      def update_save_data_options(self):
          """
          This function handles data saving dialog
          """
          print "This update"
          pass

      def display_all_CCD_settings(self):
          pass
      
      
      # start_acquisition is used for single spectrum acquisition and continuous mode
      def start_acquisition(self):

        try:
            #self.timer.start()
            self.acquire_button.setEnabled(False)
            self.stop_button.setEnabled(True)
 
            if self.PIXIS_thread.isStopped():

                if self.acquire_continuous_tick.isChecked():
                    self.timer.start(self.refresh)
                    print "setting ac mode"
                    self.PIXIS_thread.set_acquisition_mode(0)
                elif not self.acquire_continuous_tick.isChecked():
                    #self.timer.start(self.refresh)
                    self.PIXIS_thread.set_acquisition_mode(1)
                    self.stop_button.setEnabled(False)
                    self.acquire_button.setEnabled(True)
                print "starting thread"
                print self.PIXIS_thread.isRunning()
                
                self.PIXIS_thread.start()
                print "never reaching after thread_start"

                    

                print "not going after thread start"
            else:
                print "Couldn't start acquisition - already running!"

        except:
            self.pop_up = QtGui.QMessageBox()
            self.pop_up.setText("USBError: check that cable is plugged")
            self.pop_up.exec_()
            
      def stop_acquisition(self):
        if not self.PIXIS_thread.isStopped():
            #self.timer.stop()
            self.acquire_button.setEnabled(True)
            self.stop_button.setEnabled(False)
            #self.pbar.hide()
            print "going in stop_Acquisition"
            self.PIXIS_thread.resume()
            self.PIXIS_thread.stop()
            
            print "Readout rate right at the end"
            print self.PIXIS.status.readout_rate

        else:
            print "Couldn't stop acquisition - it wasn't running!"
      
      def update_data(self, new_data_ptr):
          self.data_ptr= new_data_ptr
      def plot_spectrum(self):
          #print "in plot spectrum"
          #print self.PIXIS.available.initial_readout
          #print self.PIXIS.status.readout_rate
          #print self.PIXIS.test
          #self.data= self.PIXIS.get_data(self.PIXIS.test)
          
          #print self.data_ptr
          #print np.shape(self.PIXIS.data)
          #print "numpy data"
          #print self.PIXIS.numpydata
          #print self.PIXIS_thread.isRunning()
          start= time.clock()
          
          self.PIXIS.picam.PicamAdvanced_GetAcquisitionBuffer(self.PIXIS._handle_device, ctypes.byref(self.PIXIS.buffer_cam))
            #print "shape of datat"
            #print self.buffer_cam.memory
          self.PIXIS.frame_ptr = ctypes.cast(self.PIXIS.buffer_cam.memory, ctypes.POINTER(ctypes.c_uint16*(int(0.5*2680))))
          #self.data= self.PIXIS.get_data()
          self.numpydata = np.frombuffer(self.PIXIS.frame_ptr.contents, dtype='uint16')
   
          #print elapsed
          #print time.time()
          #print self.data
          #test= np.random.rand(1340, 1)
          self.win.update_plot(self.wavelengths_acton, self.numpydata)
          elapsed= time.clock() - start
          print elapsed
          #print self.PIXIS.data
      """
      def start_acquisition(self):
          self.PIXIS.commit_params()
          # sets camera timeout in ms
          self.PIXIS_timeout=data

          try:
              #timeout does not count as an error in picam. So we measure time here.
              start_time= time.time()
              self.PIXIS.acquire(self.PIXIS_timeout,1)
              end_time= time.time()
              
              #print "acquisiton time"
              #print end_time - start_time
              #print self.PIXIS_timeout*1e-3
              
              #if end_time - start_time > self.PIXIS_timeout*1e-3:
              #       msgBox = QtGui.QMessageBox()
              #       msgBox.setWindowTitle("Problem Acquiring spectrum") 
              
                     #print self.PIXIS.error_str
              #       msgBox.setText("Timeout may have occured. Verify that trigger is plugged in if operating \n in ext. sync mode.")
              #       msgBox.exec_() 
              self.data= self.PIXIS.get_data()
              #print "size of data"
              #print np.shape(self.data)
              self.win.update_plot(self.wavelengths_acton,np.squeeze(self.data),0)
              
              #print "PIXIS error string"
              #print self.PIXIS.error_str
          except:
              msgBox = QtGui.QMessageBox()
              msgBox.setWindowTitle("Problem Acquiring spectrum") 
              #print "PIXIS error string"
              #print self.PIXIS.error_str
              
              if self.PIXIS.error_str != '':
                  print "are we going there"
                  print self.PIXIS.error_str
                  msgBox_text= self.PIXIS.error_str
              elif self.PIXIS.error_str == 'None':
                  
                  if self.PIXIS.errors.value != 0:
                      msgBox_text= "PicamAcquisitionErrorsMask value: " + str(self.PIXIS.errors)
                  elif self.PIXIS.errors.value == 0:
                      msgBox_text = "Unknown acquisition data error"
              #print self.PIXIS.error_str
              msgBox.setText(msgBox_text)
              msgBox.exec_() 
      """
      
      def load_path(self):
        """
        This function handles the loading of the wavelengths.txt file
        """
        fname = str(QtGui.QFileDialog.getOpenFileName(self, 'Open file',  '/home'))
        fname_split= fname.split('/')
        #tries to load new wavelengths.txt file. If the file does not have the right format,
        #ValueError comes up
        try:
            #Checks that the wavelengths file has the correct name
            assert fname_split[-1] == 'wavelengths_acton.txt'
            if fname!='':
                self.QLineEdit_path.setText(fname)
                #Gives the wavelengths in [nm]
                self.wavelengths_acton = np.loadtxt(fname)
    
                try:
                 #Checks that the wavelengths vector has the same size as the ROI employed   
                 assert np.size(self.wavelengths_acton) == self.PIXIS_ROI_width
                 self.update_cursors_max_boundaries()
                except AssertionError:
                    msgBox = QtGui.QMessageBox()
                    msgBox.setWindowTitle("Problem setting wavelengths") 
                    msgBox.setText("The size of wavelengths vector does not match the ROI width")
                    msgBox.exec_() 
                    print self.wavelengths_acton
                
                
        except AssertionError:
            msgBox = QtGui.QMessageBox()
            msgBox.setWindowTitle("Error loading") 
            msgBox.setText("The file cannot be loaded. Filename must be named 'wavelengths_acton.txt' ")
            msgBox.exec_()

        except ValueError:
            print "going ther"
            msgBox = QtGui.QMessageBox()
            msgBox.setWindowTitle("Error loading") 
            msgBox.setText("The file cannot be loaded. Check that it is in the 'wavelengths_acton.txt'format. ")
            msgBox.exec_()

# The Class "Spectro_thread" inherits from QThread and is responsible for all the acquistions tasks performed by the spectrometer.
class PIXIS_thread(QtCore.QThread):
    def __init__(self, lock, timeout, parent=None):

        super(PIXIS_thread, self).__init__(parent)
        self.parent=parent
        #self.spectro = pixis
        self.lock = lock
        self.stopped = True
        self.paused = False
        # mutex is used to restrict a portion of code, data structure,etc..to a single thread
        self.mutex = QtCore.QMutex()
        self.completed = False
        self.last = np.zeros(len(self.parent.wavelengths_acton))
        
        # idx_run is an index identifying the type of acquisition to be performed by the spectro.
        self.idx_run = 1

    def __del__(self):
        print "Spectro deleted"
        # The function "run" is authomatically called when the start method is called in the main programme.
        # idx_run =0 corresponds to acquiring continuously
        # idx_run =1 corresponds to acquiring nshots a single time
        # idx_run =2 corresponds to acquiring a background (requires its own index as can be seen below)

    def run(self):
        print "is the run function called?"
        self.stopped = False
        print "is the run function called?"        
        try:
            print "going after try"
            if self.idx_run == 0:
                print "Going in run function"
                self.lock.unlock()
                self.parent.PIXIS.acquire_cont()
                self.lock.lock()
                print "Are we ever going after the spectro.acquire_cont"
                #while self.stopped == False:
                    # This sleep seems to have an influence on the spectro crashing or not for shortest acquistion times
                #    time.sleep(0.001)
                #    self.last = self.spectro.measurement_sequence(1)

            elif self.idx_run == 1:
                #print "going here"
                self.parent.PIXIS.acquire(timeout,1)
                #self.data= self.parent.PIXIS.get_data()
                self.emit(QtCore.SIGNAL("spectrum_acquired()"))
                #print self.data
                #print self.parent.PIXIS.available.initial_readout
                
            self.completed = True
            self.emit(QtCore.SIGNAL("script_finished(bool)"), self.completed)
            self.stopped = True

            print "spectro run is over"    

        except:
            print "Going in error"
            pass


    def set_acquisition_mode(self, idx_run):
        self.idx_run = idx_run


    def stop(self):
        try:
            print "readout rate:", self.parent.PIXIS.status.readout_rate
            #self.mutex.lock()
            self.stopped = True
            self.parent.PIXIS.external_condition= False
            
        finally:
            self.mutex.unlock()

    def pause(self):
        #print "Acquisition paused"
        self.paused = True

    def resume(self, display=True):
        if display:
            # print "Acquisition resumed"
            self.paused = False

    def isPaused(self):
        return self.paused

    def isStopped(self):
        return self.stopped

    def check_stopped_or_paused(self):
        while True:
            if (not self.paused) or self.stopped:
                return self.stopped
            time.sleep(0.1)
            
class DisplayParamWidget(QtGui.QWidget):
    """
    Small conveniency class handling the displaying of Ordered Dicts, in which the PIXIS parameters are stored.
    """
    def __init__(self, param=None, param_name= 'param_name'):
        super(DisplayParamWidget, self).__init__()
        self.param= param
        
        self.param_name_label= QtGui.QLabel(param_name)        
        self.param_display_box = QtGui.QTextEdit()
        self.param_display_box.setMaximumHeight(100)
        self.param_display_box.setReadOnly(True)
        
        
        self.Layout= QtGui.QGridLayout()
        self.Layout.addWidget(self.param_name_label, 0, 0)
        self.Layout.addWidget(self.param_display_box, 1, 0)
        
        self.update_QTextEdit(self.param)
        
        self.setLayout(self.Layout)
        
    def update_QTextEdit(self,new_param):
        """
        Displays the parameters in a user-friendly fashion (1 param and its value per line) 
        """
        self.param=new_param
        
        param_str= ''
        for item in self.param.items():
            param_str= param_str + item[0] + ' = ' + str(item[1]) + ' \n'
            
        self.param_display_box.setText(param_str)        


if __name__ == '__main__':
    app = QtGui.QApplication(sys.argv)
    PW= PIXIS_Widget()
    sys.exit(app.exec_())
