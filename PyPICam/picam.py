# -*- coding: utf-8 -*-
"""
Created on Thu Apr 02 10:31:02 2015

@author: Helene Seiler

Copyright © 2015 Helene Seiler, McGill University

picam.py is free software: you can redistribute it and/or modify it under the terms
of the GNU General Public License version 2 as published by the Free Software
Foundation.
Odemis is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with
Odemis. If not, see http://www.gnu.org/licenses/.
"""

# This tries to support the PICam SDK from Princeton Instruments.
# This code's structures is directly inspired from pvcam.h developped by Eric Piel as well as Joe
# Lowney, Andrew Dawes' codes.
# This has only been tested on Windows for the PI PIXIS 100B (USB) camera.
# Note that PICam is only provided for 64-bits systems

from __future__ import division
import ctypes
import logging
import os
import numpy as np
import time
from collections import OrderedDict

#picam_h.py is the transcription of the C header file pycam.h based on ctypes  
from picam_h import *
#picam_advanced_h.py is the transcription of the C header file pycam_advanced.h based on ctypes 
from picam_advanced_h import *


class PICamError(Exception):
    """
    this class directly follows Eric Piel's code structure
    """
    def __init__(self, errno, strerror):
        self.args = (errno, strerror)
    def __str__(self):
        return self.args[1]

class CancelledError(Exception):
    """
    this class directly follows Eric Piel's code structure:
    raise to indicate the acquisition is cancelled and must stop
    """
    pass


class PICamDLL(ctypes.WinDLL):
    """
    this class directly follows Eric Piel's code structure:
    Subclass of CDLL specific to PICam library, which handles error codes for
    all the functions automatically.
    It works by setting a default _FuncPtr.errcheck.
    """
    def __init__(self):
        #nt is the code for Windows
        if os.name == "nt":
            ctypes.WinDLL.__init__(self, "Picam.dll")
        else:
        # Global so that other libraries can access it
        # need to have firewire loaded, even if not used
            try:
                CDLL.__init__(self, "picam.so", RTLD_GLOBAL) # This has not been tested
            except OSError:
                logging.exception("Failed to find the PICam driver. You need to "
                "check that Picam, Picc, Pidi, Pida are installed.")
                raise
        try:
            self.Picam_InitializeLibrary()
        except PICamError:
            pass # if opened several times, initialisation fails but it's all fine



    def pi_errcheck(self, result, func, args):
        """
        Analyse the return value of a call and raise an exception in case of
        error.
        Follows the ctypes.errcheck callback convention
        """
        if result !=0 : # functions return int different than zero if error occured
          
            raise PICamError(result, "Call to %s failed with error code %d:" %
                    (func.__name__, result))
                    
        return result
        
    def __getitem__(self, name):
        try:
            func = super(PICamDLL, self).__getitem__(name)
        except Exception:
            raise AttributeError("Failed to find %s" % (name,))
        func.__name__ = name
        func.errcheck = self.pi_errcheck
        return func



    def reinit(self):
        """
        Does a fast uninit/init cycle
        """
        is_initialized= c_bool()
        self.Picam_IsLibraryInitialized(byref(is_initialized))
        if is_initialized.value:
            try:
                self.Picam_UninitializeLibrary()
            except PICamError:
                pass # whatever
            try:
                self.Picam_InitializeLibrary()
            except PICamError:
                pass # whatever
            
    def __del__(self):
        try:
            self.Picam_UninitializeLibrary()
        except:
            logging.exception("Failed during PICam uninitialization")
            pass
        
class PICam():
    def __init__(self):
         """
         Initialises the device
         device (int or string): number of the device to open, as defined in
         PICam, cf scan(), or the name of the device (as in /dev/).
         Raise an exception if the device cannot be opened.
         """
         self.picam = PICamDLL()
         self._handle =  PicamHandle()
         self._camera_array= PicamHandle_ptr()
         self._camera_count= ctypes.c_int()
         self.picamID=PicamCameraID()
         self.available = PicamAvailableData()
         self.errors = PicamAcquisitionErrorsMask()
         #string containing PICamError message
         self.error_str= None
         #list containing all ROIs effective sizes (list of tupple)
         self._effective_image_size = []
         #Frame pointer (useful for continuous acquisition)
         self.frame_ptr= 0
         try:
             logging.info("Initializing camera...")
             self.open_first_camera()
         except PICamError:
            logging.info("PI camera seems connected but not responding, "
            "you might want to try turning it off and on again.")
            raise IOError("Failed to open PICam camera")
      
         self._set_static_settings()
         self._commit_params()
         
    def get_open_cameras(self): 
        """
        returns number of open cameras as well as an array containing their handles. 
        """
        self.picam.Picam_GetOpenCameras(ctypes.byref(self._camera_array), ctypes.byref(self._camera_count))
        cam_array_ptr = ctypes.cast(self._camera_array.contents, ctypes.POINTER(ctypes.c_uint16*self._camera_count.value))
        cam_array_np = np.frombuffer(cam_array_ptr.contents, dtype='uint16')
        return self._camera_count.value, cam_array_np
        
    def open_first_camera(self):
        """  
        opens the first available camera 
        """  
        self.picam.Picam_OpenFirstCamera(ctypes.byref(self._handle))
        is_connected= ctypes.c_bool()
        self.picam.Picam_IsCameraConnected(self._handle, ctypes.byref(is_connected))
        logging.info("camera connected (True is Yes):", is_connected.value)
        
    def get_SW_info(self):  
         """
         Gets all Software info 
         """
         major = ctypes.c_int()
         minor = ctypes.c_int()
         distribution = ctypes.c_int()
         release = ctypes.c_int()
         
         try:
             self.picam.Picam_GetVersion(ctypes.byref(major),ctypes.byref(minor),ctypes.byref(distribution),ctypes.byref(release))
             
         except PICamError:
             pass
         
         picam_version= str(major.value) + '.' + str(minor.value) + '.' + str(distribution.value) + ' Released: '+ str(release.value)
         return picam_version 

    def get_ID_info(self):
        """
        Gets all Camera Identification info
        """
        string_model=ctypes.c_char_p()
        string_comp=ctypes.c_char_p()        
        try:    
            self.picam.Picam_GetCameraID(self._handle,ctypes.byref(self.picamID))
            self.picam.Picam_GetEnumerationString(PicamEnumeratedType_Model, self.picamID.model, ctypes.byref(string_model))
            self.picam.Picam_GetEnumerationString(PicamEnumeratedType_ComputerInterface, self.picamID.computer_interface, ctypes.byref(string_comp))
            model=string_model.value
            comp=string_comp.value
        except PICamError:
            model="unknown"
            comp = "unknown"
            self.picamID.serial_number="unknown"
            self.picamID.sensor_name="unknown"
        
        self.picam.Picam_DestroyString(string_model)
        self.picam.Picam_DestroyString(string_comp)
        
        return model, comp, self.picamID.serial_number, self.picamID.sensor_name 
    
    def get_FW_info(self):
        """
        Gets all FirmWare info
        """                
        picamfirmware_ptr= PicamFirmware_ptr()
        firmware_count= ctypes.c_int()
        
        try:    
            self.picam.Picam_GetFirmwareDetails(ctypes.byref(self.picamID), ctypes.byref(picamfirmware_ptr), ctypes.byref(firmware_count))
            name= picamfirmware_ptr.contents.name
            detail=picamfirmware_ptr.contents.detail  

        except PICamError:
            name="unknown"
            detail="unknown"
        
        #Destroys Firmware details
        self.picam.Picam_DestroyFirmwareDetails(picamfirmware_ptr)
        return name, detail
    
    def get_sensor_info(self):
        """
        Gets all sensor-related info
        """

        try:
            sensor_info=OrderedDict()
            CcdCharacteristics=self._get_param_value(PicamParameter_CcdCharacteristics)
            sensor_info.update({'CCD Characteristics': CcdCharacteristics})
            PixelGapHeight=self._get_param_value(PicamParameter_PixelGapHeight)
            sensor_info.update({'Pixel Gap Height [um]': PixelGapHeight})
            PixelGapWidth=self._get_param_value(PicamParameter_PixelGapWidth)
            sensor_info.update({'Pixel Gap Width [um]': PixelGapWidth})
            PixelHeight=self._get_param_value(PicamParameter_PixelHeight)
            sensor_info.update({'Pixel Height [um]': PixelHeight})
            PixelWidth=self._get_param_value(PicamParameter_PixelWidth)
            sensor_info.update({'Pixel Width [um]': PixelWidth})
 
            SensorActiveHeight=self._get_param_value(PicamParameter_SensorActiveHeight)
            sensor_info.update({'Sensor Active Height': SensorActiveHeight})
            SensorActiveWidth=self._get_param_value(PicamParameter_SensorActiveWidth)
            sensor_info.update({'Sensor Active Width': SensorActiveWidth})
            
            SensorActiveRightMargin=self._get_param_value(PicamParameter_SensorActiveRightMargin)
            sensor_info.update({'Sensor Active Right Margin': SensorActiveRightMargin})
            SensorActiveLeftMargin=self._get_param_value(PicamParameter_SensorActiveLeftMargin)
            sensor_info.update({'Sensor Active Left Margin': SensorActiveLeftMargin})
            SensorActiveTopMargin=self._get_param_value(PicamParameter_SensorActiveTopMargin)
            sensor_info.update({'Sensor Active Top Margin': SensorActiveTopMargin})
            SensorActiveBottomMargin=self._get_param_value(PicamParameter_SensorActiveBottomMargin)
            sensor_info.update({'Sensor Active Botton Margin': SensorActiveBottomMargin})
            
            SensorMaskedTopMargin=self._get_param_value(PicamParameter_SensorMaskedTopMargin)
            sensor_info.update({'Sensor Masked Top Margin': SensorMaskedTopMargin})
            SensorMaskedBottomMargin=self._get_param_value(PicamParameter_SensorMaskedBottomMargin)
            sensor_info.update({'Sensor Masked Bottom Margin': SensorMaskedBottomMargin})
            
            SensorMaskedHeight=self._get_param_value(PicamParameter_SensorMaskedHeight)
            sensor_info.update({'Sensor Masked Height': SensorMaskedHeight})

            SensorSecondaryActiveHeight=self._get_param_value(PicamParameter_SensorSecondaryActiveHeight)
            sensor_info.update({'Sensor Secondary Active Height': SensorSecondaryActiveHeight})
            SensorSecondaryMaskedHeight=self._get_param_value(PicamParameter_SensorSecondaryMaskedHeight)
            sensor_info.update({'Sensor Secondary Masked Height': SensorSecondaryMaskedHeight})
            SensorType=self._get_param_value(PicamParameter_SensorType)

            if SensorType==1:
                sensor_info.update({'Sensor Type': 'CCD'})
            elif SensorType==2:
                sensor_info.update({'Sensor Type': 'InGaAs'})

            #"ADD SENSOR LAYOUT STUFF"
            
            return sensor_info

        except PICamError:
            pass
        
        
        
    def get_temperature_info(self):
        """
        Gets all sensor temperature related info 
        """ 
        SensorTemperatureStatus=PicamEnumeratedType()
     
        try:
            SensorTemperatureStatus=self._get_param_value(PicamParameter_SensorTemperatureStatus)
            if SensorTemperatureStatus == 1: 
                temp_status = 'PicamSensorTemperatureStatus_Unlocked: The temperature has not stabilized at the set point'
            elif SensorTemperatureStatus ==2:
                temp_status = 'PicamSensorTemperatureStatus_Locked: The temperature has stabilized at the set point' 
                
            temp_reading=self._get_param_value(PicamParameter_SensorTemperatureReading) 
        
            return temp_status, temp_reading
        except PICamError:
            pass

        
    def get_all_info(self):
        """
        This function is a wrapper on some smaller get_info type of functions.
        It stores all the information in a ordereddict.
        """
        try:
            param_list=OrderedDict()
            sw_info= self.get_SW_info()
            param_list.update({'Software Info': sw_info})
            name, detail= self.get_FW_info()
            param_list.update({'Name': name})
            param_list.update({'Detail':detail})
            model, comp, serial_number, sensor_name= self.get_ID_info()
            param_list.update({'Model': model})
            param_list.update({'Comp': comp})
            param_list.update({'Serial Number': serial_number})
            param_list.update({'sensor_name': sensor_name})
            sensor_info= self.get_sensor_info()
            param_list.update({'Sensor Info': sensor_info})
            temp_status, temp_reading = self.get_temperature_info()
            param_list.update({'Temperature status': temp_status})
            param_list.update({'Temperature [F]': temp_reading})
            
            return param_list
        except PICamError:
            pass
    
    
    def _get_param_constraint(self, param, category=PicamConstraintCategory_Recommended):
        """
        This function can be called with any parameter to know the constraints on that parameter. It returns an ordered dict
        containing all the info. This is a low-level wrapper that sould be used to check the validity of user-set values in other 
        methods (such as those for exposure time settings, etc...)
        
        - category can be: PicamConstraintCategory_Recommended, PicamConstraintCategory_Capable, PicamConstraintCategory_Required
        
        Did not implement constraint types "Pulse" and "Modulations" as those do not come up in PIXIs
        """
        picamconstraint=None
        try: 
            constraint_info= OrderedDict()
            # The following constraint type exist: None (1), Range (2), Collections (3), ROIs (4), pulse (5), modulation (6)
            constraint_type=ctypes.c_int()
            self.picam.Picam_GetParameterConstraintType(self._handle, ctypes.c_int(param), ctypes.byref(constraint_type))
            
            if constraint_type.value==1:
                pass
                              
            elif constraint_type.value==2:
                picamconstraint=PicamRangeConstraint_ptr() 
                self.picam.Picam_GetParameterRangeConstraint(self._handle, ctypes.c_int(param), category, ctypes.byref(picamconstraint)  )
            
                for field_name, field_type in picamconstraint.contents._fields_:
                    constraint_info.update({field_name :getattr(picamconstraint.contents, field_name)  })
                    
                self.picam.Picam_DestroyRangeConstraints(picamconstraint)  
                
            elif constraint_type.value==3:
                picamconstraint=PicamCollectionConstraint_ptr() 
                self.picam.Picam_GetParameterCollectionConstraint(self._handle, ctypes.c_int(param), category, ctypes.byref(picamconstraint)  )
            
                for field_name, field_type in picamconstraint.contents._fields_:
                    constraint_info.update({field_name :getattr(picamconstraint.contents, field_name)  })
               
                self.picam.Picam_DestroyCollectionConstraints(picamconstraint)
        
                
            elif constraint_type.value==4:
                picamconstraint=PicamRoisConstraint_ptr()
                self.picam.Picam_GetParameterRoisConstraint(self._handle, ctypes.c_int(param), category, ctypes.byref(picamconstraint)  )
                    
                for field_name, field_type in picamconstraint.contents._fields_:
                    constraint_info.update({field_name :getattr(picamconstraint.contents, field_name)  })
                
                self.picam.Picam_DestroyRoisConstraints(picamconstraint)
                
            elif param_type.value==7 or param_type.value==8:
                #TODO: implement constraints for PUlse and modulation
                pass
            else:
                raise NotImplementedError("Argument of unknown type %d" % param_type.value)          
            
           
        except PICamError:
            pass
            
        return constraint_info
    
    def _get_param_value(self, param):
        """
        This function can be called with any PicamParameter available in the picam_h.py module to know the value of that parameter.
        This is a low-level wrapper.
        
        Did not implement constraint types "Pulse" and "Modulations" as those do not come up in PIXIs
        """
        param_value=None        
        try:
            param_type=ctypes.c_int()
            self.picam.Picam_GetParameterValueType(self._handle, param, ctypes.byref(param_type))  
            
            if param_type.value==1 or param_type.value==4:
               param_value=ctypes.c_int()
               self.picam.Picam_GetParameterIntegerValue(self._handle, ctypes.c_int(param), ctypes.byref(param_value))
               return param_value.value
            elif param_type.value==2:
               param_value=ctypes.c_double()
               self.picam.Picam_GetParameterFloatingPointValue(self._handle, ctypes.c_int(param), ctypes.byref(param_value)) 
               return param_value.value
            elif param_type.value==3:
               param_value=ctypes.c_bool()
               self.picam.Picam_GetParameterIntegerValue(self._handle, ctypes.c_int(param), ctypes.byref(param_value)) 
               return param_value.value
            elif param_type.value==5:
               param_value=PicamRois_ptr()   
               self.picam.Picam_GetParameterRoisValue(self._handle,ctypes.c_int(param), ctypes.byref(param_value))
               return param_value.contents
            elif param_type.value==6:
               param_value= ctypes.c_int64()
               self.picam.Picam_GetParameterLargeIntegerValue(self._handle, ctypes.c_int(param), ctypes.byref(param_value)) 
               return param_value.value   
            elif param_type.value==7 or param_type.value==8:
                pass
            else:
                raise NotImplementedError("Argument of unknown type %d" % param_type.value)
                
        except PICamError:
            pass
        
    def _set_param_value(self, param, param_value):
        """
        This function can be called to set the value of any PicamParameter available in the picam_h.py module.
        This is a low-level wrapper and should be used as a building block for more user-friendly functions (example:
        set_exposure_time). A test is carried out to check if the desired param_value is included in the range of possible values.
        
        Did not implement constraint types "Pulse" and "Modulations" as those do not come up in PIXIs
        """
        is_settable= ctypes.c_bool(False)   
        try:
            param_type= ctypes.c_int()
            self.picam.Picam_GetParameterValueType(self._handle, param, ctypes.byref(param_type))  
            if param_type.value==1 or param_type.value==4:
               param_value_ctypes= ctypes.c_int(param_value)
               self.picam.Picam_CanSetParameterIntegerValue(self._handle, ctypes.c_int(param), param_value_ctypes, ctypes.byref(is_settable) )
               if is_settable.value==True:
                   self.picam.Picam_SetParameterIntegerValue(self._handle, ctypes.c_int(param), param_value_ctypes)
               else:
                   raise ValueError("Cannot set parameter to desired value")
               
            elif param_type.value==2:
               param_value_ctypes=ctypes.c_double(param_value)
               self.picam.Picam_CanSetParameterFloatingPointValue(self._handle, ctypes.c_int(param), param_value_ctypes, ctypes.byref(is_settable) )                
               if is_settable.value==True:
                   self.picam.Picam_SetParameterFloatingPointValue(self._handle, ctypes.c_int(param), param_value_ctypes) 
               else:
                   raise ValueError("Cannot set parameter to desired value")
               
            elif param_type.value==3:
               param_value_ctypes=ctypes.c_bool(param_value)
               self.picam.Picam_CanSetParameterIntegerValue(self._handle, ctypes.c_int(param), param_value_ctypes, ctypes.byref(is_settable) )
               if is_settable.value==True:
                   self.picam.Picam_SetParameterIntegerValue(self._handle, ctypes.c_int(param), param_value_ctypes)
               else:
                   raise ValueError("Cannot set parameter to desired value")
       
            elif param_type.value==5:
               param_value_ctypes=ctypes.byref(param_value)
               self.picam.Picam_CanSetParameterRoisValue(self._handle, ctypes.c_int(param), param_value_ctypes, ctypes.byref(is_settable) )
               if is_settable.value==True:
                   self.picam.Picam_SetParameterRoisValue(self._handle, ctypes.c_int(param), param_value_ctypes)
               else:
                   raise ValueError("Cannot set parameter to desired value")
              

            elif param_type.value==6:
               param_value_ctypes=ctypes.c_int64(param_value)
               self.picam.Picam_CanSetParameterLargeIntegerValue(self._handle, ctypes.c_int(param), param_value_ctypes, ctypes.byref(is_settable))
               if is_settable.value==True:
                   self.picam.Picam_SetParameterLargeIntegerValue(self._handle, ctypes.c_int(param), param_value_ctypes) 
              
            elif param_type.value==7 or param_type.value==8:
                pass
            else:
                raise NotImplementedError("Argument of unknown type %d" % param_type.value)
                
        except PICamError:
            pass
        
        
    def _commit_params(self):
        """
        This function should be called in any higher-level function after setting PicamParameters to new values
        using _set_param_value. If this method is not called, the new parameters will not apply.
        
        Did not implement constraint types "Pulse" and "Modulations" as those do not come up in PIXIs
        """
        try:
            paramsFailed=ctypes.pointer((ctypes.c_int)())
            failCount=ctypes.c_int()
            are_committed=ctypes.c_bool(False)
            self.picam.Picam_AreParametersCommitted(self._handle,ctypes.byref(are_committed))
            if are_committed.value==False:
                self.picam.Picam_CommitParameters(self._handle, ctypes.byref(paramsFailed), ctypes.byref(failCount))
                if failCount.value !=0:
                    return failCount.value, paramsFailed
            self.picam.Picam_DestroyParameters(paramsFailed)
        except PICamError:
            pass
        
    def _get_nparray_from_ctypesarray(self, memory_adress, array_size, data_type): 
        """ Create an instance of the pointer type, and point it to initial readout contents (memory address?) """
        #DataPointer = ctypes.cast(picamconstraint.contents.values_array,DataArrayPointerType)
        DataPointer= ctypes.cast(memory_adress, ctypes.POINTER(data_type*array_size))
        numpydata = np.frombuffer(DataPointer.contents, dtype='float')
        return numpydata
    
    def reinitialize(self):
        """
        Waits for the camera to reappear and reinitialise it. Typically
        useful in case the user switched off/on the camera.
        """
        #TODO: test this function!
        try:
            self.picam.Picam_Close_Camera(self._handle)
        except PICamError:
            pass
        self._handle = PicamHandle()
    
        # TEST ON PICAM PVCam only update the camera list after uninit()/init()
        while True:
            logging.info("Waiting for the camera to reappear")
            self.picam.reinit()
            try:
                self.picam.Picam_OpenFirstCamera(ctypes.byref(self._handle))
                break # succeeded!
            except PVCamError:
                time.sleep(1)
        
        logging.info("Reinitialisation successful")
 
        
    def _set_static_settings(self):
        """ 
        Set up all the values that we don't need to change after.
        Should only be called at initialisation
        
        In our case the static settings are:
        - ACD settings
        - Trigger settings
        - LogicOut settings
        - Exposure and shutter settings
        - Readout Controls
        - sensor cleaning
        
        """
        pass
    
    def set_shutter_timing(self, exp_time, shutterclosingdelay, shutterdelayresolution, shuttertimingmode):
        """
        This function sets shutter timing parameters. 
        Only compatible with PIXIS and Quad RO. For other models you will have to write another function.
        """

        try:
            self.exp_time= exp_time
            self.shutterclosingdelay= shutterclosingdelay
            self.shutterdelayresolution= shutterdelayresolution
            self.shuttertimingmode= shuttertimingmode
            self._set_param_value(PicamParameter_ExposureTime, self.exp_time)
            self._set_param_value(PicamParameter_ShutterClosingDelay, self.shutterclosingdelay)
            self._set_param_value(PicamParameter_ShutterDelayResolution, self.shutterdelayresolution)
            self._set_param_value(PicamParameter_ShutterTimingMode, self.shuttertimingmode)

        except PICamError:
            pass
        
    def get_shutter_timing(self):
        """
        Conveniency function
        """
        
        try:
            shutterclosingdelay= self._get_param_value(PicamParameter_ShutterClosingDelay)
            shutterdelayresolution= self._get_param_value(PicamParameter_ShutterDelayResolution)
            shuttertimingmode= self._get_param_value(PicamParameter_ShutterTimingMode)
        
            return shutterclosingdelay, shutterdelayresolution, shuttertimingmode
            
        except PICamError:
            pass
   
    def set_exposure_time(self, exp_time):
        """
        Conveniency function.Just sets the exposure time. 
        """
        try:
            self.exp_time= exp_time
            self._set_param_value(PicamParameter_ExposureTime, self.exp_time)
            
        except PICamError:
            pass
    #ADC stuff
            
    def get_exposure_time(self):
        """
        Conveniency function. Just gets the exposure time.
        """        
        try:
            return self._get_param_value(PicamParameter_ExposureTime)
        except PICamError:
            pass             
        
    def set_Adc_params(self, AdcAnalogGain, AdcBitDepth, AdcQuality, AdcSpeed):
        """
        ADC SETTINGS
        This function sets Adc parameters.
        Only compatible with PIXIS and PYlon CCDs. For other models you will have to write another function.
        """
        try:
            self.AdcAnalogGain= AdcAnalogGain
            self.AdcBitDepth= AdcBitDepth
            self.AdcQuality= AdcQuality
            self.AdcSpeed= AdcSpeed
            self._set_param_value(PicamParameter_AdcAnalogGain, self.AdcAnalogGain)
            self._set_param_value(PicamParameter_AdcBitDepth, self.AdcBitDepth)
            self._set_param_value(PicamParameter_AdcQuality, self.AdcQuality)
            self._set_param_value(PicamParameter_AdcSpeed, self.AdcSpeed)
            
        except PICamError:
            pass
    

    def get_Adc_params(self):
        """
        returns all IO related params
    
        -   gain: is the set of electronic gain settings for pixel digitization (can be high (1), low (2) or medium (3))
        -   bitdepth:  Controls the resolution of the pixel digitization in bits-per-pixe 
        -   quality:  is the set of Analog-to-Digital conversion techniques and quality settings 
            for pixel digitization (PicamAdcQuality_ElectronMultiplied: Provides electron multiplication, PicamAdcQuality_HighCapacity:
            Optimized for sensing high levels of radiation, PicamAdcQuality_HighSpeed: Provides faster readout speeds,
            PicamAdcQuality_LowNoise: Optimized for the lowest noise.)
        -   speed: Controls the rate pixels are digitized in MHz
            
        This function has only been tested with a PIXIS camera. It may need to be adapted for other types of devices.
        """
        try:
            gain= self._get_param_value(PicamParameter_AdcAnalogGain)
            bitdepth= self._get_param_value(PicamParameter_AdcBitDepth)
            quality= self._get_param_value(PicamParameter_AdcQuality)
            speed= self._get_param_value(PicamParameter_AdcSpeed)
        
            return gain, bitdepth, quality, speed
            
        except PICamError:
            pass
    # I/O Stuff
    def set_IO_settings(self, response=PicamTriggerResponse_NoResponse, 
                       determination=PicamTriggerDetermination_RisingEdge,
                       signalOUT=PicamOutputSignal_NotReadingOut):
        """  
        I/O SETTINGS
        -   response: set of camera responses to an external trigger (PicamTriggerResponse_NoResponse
            PicamTriggerResponse_ExposeDuringTriggerPulse, PicamTriggerResponse_ReadoutPerTrigger,
            PicamTriggerResponse_ShiftPerTrigger, StartOnSingleTrigger)
        -   determination: external trigger styles that are recognized by a camera (PicamTriggerDetermination_PositivePolarity,
            PicamTriggerDetermination_NegativePolarity, PicamTriggerDetermination_RisingEdge, PicamTriggerDetermination_FallingEdge)
        -   signalOUT: set of parameters defining a camera's MONITOR OUTPUT signal (PIXIS: PicamOutputSignal_NotReadingOut,
            PicamOutputSignal_ShutterOpen, PicamOutputSignal_Busy, PicamOutputSignal_AlwaysLow,
            PicamOutputSignal_AlwaysHigh)
            
        This function only works for PIXIS and QUAD RO cameras. It needs to be adapted for other types of devices.
        """                 
        try:
           self.triggerresponse= response
           self.triggerdetermination=determination
           self.signalOUT= signalOUT
           self._set_param_value(PicamParameter_TriggerResponse,self.triggerresponse) 
           self._set_param_value(PicamParameter_TriggerDetermination,self.triggerdetermination) 
           self._set_param_value(PicamParameter_OutputSignal ,self.signalOUT) 
        
        except PICamError:
            pass
        
    def get_IO_settings(self):
        """      
        returns all IO related params
    
        -   response: set of camera responses to an external trigger (PicamTriggerResponse_NoResponse
            PicamTriggerResponse_ExposeDuringTriggerPulse, PicamTriggerResponse_ReadoutPerTrigger,
            PicamTriggerResponse_ShiftPerTrigger, StartOnSingleTrigger)
        -   determination: external trigger styles that are recognized by a camera (PicamTriggerDetermination_PositivePolarity,
            PicamTriggerDetermination_NegativePolarity, PicamTriggerDetermination_RisingEdge, PicamTriggerDetermination_FallingEdge)
        -   signalOUT: set of parameters defining a camera's MONITOR OUTPUT signal (PIXIS: PicamOutputSignal_NotReadingOut,
            PicamOutputSignal_ShutterOpen, PicamOutputSignal_Busy, PicamOutputSignal_AlwaysLow,
            PicamOutputSignal_AlwaysHigh)
            
        This function only works for PIXIS, ProEM cameras. It needs to be adapted for other types of devices.
        """
        try:
           response= self._get_param_value(PicamParameter_TriggerResponse) 
           determination= self._get_param_value(PicamParameter_TriggerDetermination) 
           signalOUT= self._get_param_value(PicamParameter_OutputSignal) 
           
           return response, determination, signalOUT
                   
        except PICamError:
            pass 
        
    # Readout control stuff 
    def set_readout_controls(self,  verticalshiftrate, readoutcontrolmode= PicamReadoutControlMode_FullFrame, kineticswindowheight=1):
        """      
        READOUT CONTROL SETTINGS
        verticalshiftrate: Controls the rate to shift one row towards the serial register in a CCD in microseconds. (uS)
        readoutcontrolmode: controls how the sensor is read. Can be full frame, frametransfer, kinetics, ...
        kineticswindowheight: Controls the number of rows used for the sensing window in a kinetics readout.
        This function only works for PIXIS, Pylon and ProEM cameras. It needs to be adapted for other types of devices.
        """
        try:
           self.kineticswindowheight= kineticswindowheight
           self.readoutcontrolmode= readoutcontrolmode
           self.verticalshiftrate= verticalshiftrate
           self._set_param_value(PicamParameter_KineticsWindowHeight , self.kineticswindowheight)
           self._set_param_value(PicamParameter_ReadoutControlMode, self.readoutcontrolmode)
           self._set_param_value(PicamParameter_VerticalShiftRate, self.verticalshiftrate)

        except PICamError:
            pass
    
    # Data acquisition stuff
    def set_data_acquisition(self, ):
        
        pass
    
    # Sensor Layout stuff
    def set_sensor_layout(self, activebottommargin, activeheight, activeleftmargin,
                          activerightmargin, activetopmargin,activewidth):
        """      
        SENSOR LAYOUT CONTROLS
        
        -   activeheight: controls the number of active rows
        -   activewidth: controls the number of active columns
        -   activeleftmargin: controls the inactive number of columns on the left
        -   activerightmargin: controls the inactive number of columns on the right
        -   activebottommargin: controls the inactive number of columns on the bottom
        -   activetopmargin: controls the inactive number of columns on the top
      
        This function only works for PIXIS, ProEM cameras. It needs to be adapted for other types of devices.
        """
        try:
           self.activebottomargin= activebottomargin
           self.activeheight= activeheight
           self.activeleftmargin= activeleftmargin
           self.activerightmargin= activerightmargin
           self.activetopmargin= activetopmargin
           self.activewidth= activewidth
           self._set_param_value(PicamParameter_ActiveBottomMargin, self.activebottommargin)
           self._set_param_Value(PicamParameter_ActiveHeight, self.activeheight)
           self._set_param_value(PicamParameter_ActiveLeftMargin, self.activeleftmargin)
           self._set_param_value(PicamParameter_ActiveRightMargin, self.activerightmargin)
           self._set_param_Value(PicamParameter_ActiveTopMargin, self.activetopmargin)
           self._set_param_value(PicamParameter_ActiveWidth, self.activewidth)
           
        except PICamError:
            pass                     
     
     
    # Sensor cleaning stuff
    def set_sensorcleaning_params(self,  cleancyclecount, cleancycleheight,
                            cleansectionfinalheight, cleansectionfinalheightcount,
                            cleanserialregister, cleanuntiltrigger):
        """      
        SENSOR CLEANING CONTROLS
        
        -   cleancyclecount: Controls the number of clean cycles to run before acquisition begins.
        -   cleancycleheight: Controls the number of rows in a clean cycle.
        -   cleansectionfinalheight: Controls the final height rows for exponential decomposition cleaning.
        -   cleansectionfinalheightcount: Controls the final height iterations for exponential decomposition cleaning
        -   cleanserialregister: Controls the cleaning of the serial register itself.
        -   cleanuntiltrigger: Controls the nature of cleaning while waiting for an external trigger
      
        This function only works for PIXIS, ProEM cameras. It needs to be adapted for other types of devices.
        """
        try:
           self.cleancyclecount= cleancyclecount
           self.cleancycleheight= cleancycleheight
           self.cleansectionfinalheight= cleansectionfinalheight
           self.cleansectionfinalheightcount= cleansectionfinalheightcount
           self.cleanserialregister=  cleanserialregister
           self.cleanuntiltrigger= cleanuntiltrigger
      
           self._set_param_value(PicamParameter_CleanCycleCount, self.cleancyclecount)
           self._set_param_value(PicamParameter_CleanCycleHeight, self.cleancycleheight)
           self._set_param_value(PicamParameter_CleanSectionFinalHeight, self.cleansectionfinalheight)
           self._set_param_value(PicamParameter_CleanSectionFinalHeightCount, self.cleansectionfinalheightcount)
           self._set_param_value(PicamParameter_CleanSerialRegister, self.cleanserialregister)
           self._set_param_value(PicamParameter_CleanUntilTrigger, self.cleanuntiltrigger)
           
        except PICamError:
            pass                     
                             
    def get_sensorcleaning_params(self):
        """      
        returns all sensor cleaning related params
    
        -   cleancyclecount: Controls the number of clean cycles to run before acquisition begins.
        -   cleancycleheight: Controls the number of rows in a clean cycle.
        -   cleansectionfinalheight: Controls the final height rows for exponential decomposition cleaning.
        -   cleansectionfinalheightcount: Controls the final height iterations for exponential decomposition cleaning
        -   cleanserialregister: Controls the cleaning of the serial register itself.
        -   cleanuntiltrigger: Controls the nature of cleaning while waiting for an external trigger
      
        This function only works for PIXIS, ProEM cameras. It needs to be adapted for other types of devices.
        """
        try:
           cleancyclecount= self._get_param_value(PicamParameter_CleanCycleCount)
           cleancycleheight= self._get_param_value(PicamParameter_CleanCycleHeight)
           cleansectionfinalheight= self._get_param_value(PicamParameter_CleanSectionFinalHeight)
           cleansectionfinalheightcount= self._get_param_value(PicamParameter_CleanSectionFinalHeightCount)
           cleanserialregister= self._get_param_value(PicamParameter_CleanSerialRegister)
           cleanuntiltrigger= self._get_param_value(PicamParameter_CleanUntilTrigger)
           
           return cleancyclecount, cleancycleheight, cleansectionfinalheight, cleansectionfinalheightcount, cleanserialregister, cleanuntiltrigger
                   
        except PICamError:
            pass   
    
    def set_time_stamp(self, timestamps = PicamTimeStampsMask_None,
                       timestampbitdepth = 64, timestampresolution=100):
        """
        sets time stamps properties
        - timesteampbitdepth: Controls time stamp metadata via the PicamTimeStampsMask data enumeration.
        - timestampresolution: Controls the time stamp resolution in ticks-per-second (computer-dependent).
        - timestamps: Controls the time stamp size in bits-per-pixel (Because time stamps may be negative one bit is 
            reserved for sign.) Can be PicamTimeStampsMask_None, PicamTimeStampsMask_ExposureStarted, PicamTimeStampsMask_ExposureEnded
        """ 
        try:   
            self.timestamps= timestamps
            self.timestampbitdepth= timestampbitdepth
            self.timestampresolution= timestampresolution
            self._set_param_value(PicamParameter_TimeStampBitDepth, self.timestampbitdepth)
            self._set_param_value(PicamParameter_TimeStampResolution, self.timestampresolution)
            self._set_param_value(PicamParameter_TimeStamps, self.timestamps)
        
        except PICamError:
            pass
    
        
    def set_frame_tracking(self, trackframe= False, trackframebitdepth=64):
        """
        sets frame tracking properties
        - trackframe: Controls frame tracking metadata.
        - trackframebitdepth: Controls the frame tracking number size in bits-per-pixel.
        """ 
        try: 
            self.trackframe_bln= trackframe
            self.trackframebitdepth= trackframebitdepth                         
            self._set_param_value(PicamParameter_TrackFrames, self.trackframe_bln)
            self._set_param_value(PicamParameter_FrameTrackingBitDepth, self.trackframebitdepth)
            
        except PICamError:
            pass
        
    def add_ROI(self, s1, s2, sbin, p1, p2, pbin):
        
        try:         
            self.rois= self._get_param_value(PicamParameter_Rois)
            roi_idx=self.rois.roi_count            
            self.rois.roi_array[roi_idx -1].s1 = s1
            self.rois.roi_array[roi_idx -1].s2 = s2
            self.rois.roi_array[roi_idx- 1].sbin= sbin
            self.rois.roi_array[roi_idx -1].p1 = p1
            self.rois.roi_array[roi_idx -1].p2 = p2
            self.rois.roi_array[roi_idx -1].pbin= pbin

    
            #Performs binning on the y axis (max possible binning, for spectroscopy applications)   
            #self.rois.roi_array[0].pbin= self.rois.roi_array[0].p2
            #sets the ROI

            self._set_param_value(PicamParameter_Rois, self.rois)
            self._commit_params()
            #the following lines are needed for data_retrieval process
            effective_width=int((self.rois.roi_array[roi_idx -1].s2 -self.rois.roi_array[roi_idx -1].s1) /self.rois.roi_array[roi_idx -1].sbin)
            effective_height=int((self.rois.roi_array[roi_idx -1].p2 -self.rois.roi_array[roi_idx -1].p1) /self.rois.roi_array[roi_idx -1].pbin)
            self._effective_image_size.append((effective_width, effective_height))
        except PICamError:
            pass
            
    def get_read_only_acquisition_settings(self):        
        """
        gets all the acquisition parameters which are read-only parameters. Stores them in
        an ordered Python dictionary. For information purposes only.
    
        should work for all CCDs but has only been tested with PIXIS. 
        """ 
        try:
            self.framesize= self._get_param_value(PicamParameter_FrameSize)
            self.framesperreadout= self._get_param_value(PicamParameter_FramesPerReadout)
            self.framestride= self._get_param_value(PicamParameter_FrameStride)
            self.frameratecalculation= self._get_param_value(PicamParameter_FrameRateCalculation)
            self.exactreadoutcountmaximum= self._get_param_value(PicamParameter_ExactReadoutCountMaximum)
            self.readoutstride= self._get_param_value(PicamParameter_ReadoutStride)
            self.readouttimecalculation= self._get_param_value(PicamParameter_ReadoutTimeCalculation)
            self.readoutratecalculation= self._get_param_value(PicamParameter_ReadoutRateCalculation)
            self.onlinereadoutratecalculation= self._get_param_value(PicamParameter_OnlineReadoutRateCalculation)
            self.pixelbitdepth= self._get_param_value(PicamParameter_PixelBitDepth)
            self.orientation= self._get_param_value(PicamParameter_Orientation)
            
            RO_acq_settings=OrderedDict()
            #Each pixel has two bytes. Framesize is given in bytes.
            RO_acq_settings.update({'Frame Size [pixel]:' : self.framesize/2})
            RO_acq_settings.update({'Frame Per Readout:' : self.framesperreadout})
            RO_acq_settings.update({'Frame Stride [bytes]:' : self.framestride})
            RO_acq_settings.update({'Frame Rate Calculation [Frames/s]': self.frameratecalculation})
            RO_acq_settings.update({'Exact Readout Count Maximum:' : self.exactreadoutcountmaximum})
            RO_acq_settings.update({'Readout Stride [bytes]:' : self.readoutstride})
            RO_acq_settings.update({'Readout Time Calculation [s]:' : self.readouttimecalculation})
            RO_acq_settings.update({'Readout Rate Calculation []:' : self.readoutratecalculation}) 
            RO_acq_settings.update({'Online Readout Rate Calculation [?]:' : self.onlinereadoutratecalculation})
            RO_acq_settings.update({'Pixel Bit Depth:' : self.pixelbitdepth})
            RO_acq_settings.update({'Orientation:' : self.orientation})
            
            return RO_acq_settings
            
        except PICamError:
            pass
        
    def acquire(self, time_out=-1, N=1):
        """
        This is the basic acquisition function. The user specifies a time_out period (-1 is no timeout) and N is the
        number of spectra (the maximum number of spectra is given by ExactReadoutCountMaximum)
        """
        self.readout_count = ctypes.c_int(N)
        self.readout_time_out= ctypes.c_int(time_out)
        #this commit should not be necessary but is added for the sake of making sure that
        #all the PiCamParameters changed by the user (especially by employing low level functions directly) are updated.
        self._commit_params()
  
        try:        
            self.picam.Picam_Acquire(self._handle, self.readout_count, self.readout_time_out, ctypes.byref(self.available), ctypes.byref(self.errors))    
        except PICamError:
            pass
        
        self.trs=self._get_param_value(PicamParameter_TimeStampResolution)

        
    def get_data(self):
        """
        This is the function used to retrieve data after the acquire function has been called.
        In the current implementation, only data of the first ROI are retrieved. 
        The function also returns metadata in the form of two lists: ts, which contains timestamps,
        and ft, which contains frametracking data.
        """
        
        #Size of array should be Readout stride bytes/2 as each pixel has two bytes
        self.readout_stride= self._get_param_value(PicamParameter_ReadoutStride)
        self.readout_count=self._get_param_value(PicamParameter_ReadoutCount)

        if self.readout_count == 0:
            self.no_of_spectra=1
        else:
            self.no_of_spectra= self.readout_count
        
        self.data = np.zeros((self._effective_image_size[0][0], self._effective_image_size[0][1],self.no_of_spectra))
        self.frame_size= self._get_param_value(PicamParameter_FrameSize)
        self.frame_tracking_bit_depth= self._get_param_value(PicamParameter_FrameTrackingBitDepth)
        self.time_stamp_bit_depth= self._get_param_value(PicamParameter_TimeStampBitDepth)
        
        #this is the timestamps vector used to keep track of acquisition time as a function of shots
        ts=[]
        #this is the frametracking vector
        ft=[]
        for i in range(self.readout_count):
            frame_ptr = ctypes.cast(self.available.initial_readout + self.readout_stride*i, ctypes.POINTER(ctypes.c_uint16*(int(0.5*self.frame_size))))
            time_stamp_ptr= ctypes.cast(self.available.initial_readout+ self.readout_stride*(i+1) - 16,ctypes.POINTER(ctypes.c_uint64))
            frame_tracking_ptr= ctypes.cast(self.available.initial_readout + self.readout_stride*(i+1) -8 ,  ctypes.POINTER(ctypes.c_uint64))

            time_stamp_np=np.frombuffer(time_stamp_ptr.contents, dtype='uint64')
            ts.append(time_stamp_np)
            
            frame_tracking_np=np.frombuffer(frame_tracking_ptr.contents, dtype='uint64')
            ft.append(frame_tracking_np)

            data_np = np.frombuffer(frame_ptr.contents, dtype='uint16')  
            self.data[:,:,i] = np.reshape(data_np,self._effective_image_size[0])
  
        return self.data, ts
    
    def setup_acquire_cont(self):

       ptr_handletype= ctypes.c_int()
       
       self._handle_device= ctypes.c_int()
       
       self.data_np = np.zeros((self._effective_image_size[0][0]*self._effective_image_size[0][1]*1))
       #print "data address"
       data_adress= self.data_np.ctypes.data
       #print "Nbytes in array"
       nbytes= self.data_np.nbytes
       #print nbytes
       self.picam.PicamAdvanced_GetCameraDevice(self._handle, ctypes.byref(self._handle_device))
       self.picam.PicamAdvanced_GetHandleType(self._handle_device, ctypes.byref(ptr_handletype))
       #print "Handle type is:"
       #print ptr_handletype.value
       self.buffer_cam= PicamAcquisitionBuffer() 
       self.buffer_cam.memory= ctypes.c_void_p(data_adress)
       self.buffer_cam.memory_size= ctypes.c_int64(nbytes)
       self.picam.PicamAdvanced_SetAcquisitionBuffer(self._handle_device,ctypes.byref(self.buffer_cam) )
       #print "Readout Count"
       self._set_param_value(PicamParameter_ReadoutCount, 0)
       self._commit_params()
       self._get_param_value(PicamParameter_ReadoutCount)
       
    
    def acquire_cont(self):
       self.setup_acquire_cont()
       self.status= PicamAcquisitionStatus()
       self.status.running=True
       self.external_condition= True
       #self.picam.Picam_WaitForAcquisitionUpdate(self._handle, ctypes.c_int(1000), ctypes.byref(self.available), ctypes.byref(status))
       #print status.running
       print 'Start Acquisition', self.picam.Picam_StartAcquisition(self._handle)
       self.test=0
       while self.status.running==True:
            start = time.clock()
            self.picam.Picam_WaitForAcquisitionUpdate(self._handle, ctypes.c_int(1000), ctypes.byref(self.available), ctypes.byref(self.status))
            end= time.clock()
            #print end - start            
            #print 'Is running: ', status.running
            #print 'Error:', status.errors
            #self.test = self.available.initial_readout
            #print 'Readout Rate: ', self.test
            #print self.available.initial_readout
            #data_ptr= self.available.initial_readout
            #print "type of data_ptr"
            #print type(data_ptr)
            #print type(self.available.initial_readout)
            #print self.available.readout_count
            #self.emit(QtCore.SIGNAL("available_readout(int)"), data_ptr)
            #self.picam.PicamAdvanced_GetAcquisitionBuffer(self._handle_device, ctypes.byref(self.buffer_cam))
            #print "shape of datat"
            #print self.buffer_cam.memory
            #self.frame_ptr = ctypes.cast(self.buffer_cam.memory, ctypes.POINTER(ctypes.c_uint16*(int(0.5*2680))))
             
            #print self.numpydata
            
            if self.external_condition == False:
               print 'Stop Acquisition', self.picam.Picam_StopAcquisition(self._handle)
  
            #if status.running==False:
            #  print 'Problem acquiring: ', self.picam.Picam_WaitForAcquisitionUpdate(self._handle, ctypes.c_int(1000), ctypes.byref(self.available), ctypes.byref(status))  
       print "running?"
       print self.status.running

    def stop_cont_acquisition(self):
        print 'Stop ACqusition', self.picam.Picam_StopAcquisition(self._handle)
    #Have an acquire thread
    #Understand threading.locking
        
        
    def terminate(self):
        """
        releases all resources that have been associated with a specified
        camera
        handle:
        """
        #TODO: close properly for special handles
        # Frees memory for all the arrays that have been created
        self.picam.Picam_DestroyHandles(self._camera_array)
        self.picam.Picam_DestroyRois(self.rois)
        if self._handle is not None:
            logging.debug("Closing down the camera")
            print "Camera is closing"
            self.picam.Picam_CloseCamera(self._handle)
            del self.picam
            
    
    
    def __del__(self):
        self.terminate()
          
          
if __name__ == '__main__':
     #console = logging.StreamHandler()
     #console.setLevel(logging.INFO)
     #logging.getLogger('').addHandler(console)
    
     cam= PICam() 
     #print "Camera constraints"
     #print cam.get_param_constraint(PicamParameter_ExposureTime)
     #cam.set_IO_settings(response= PicamTriggerResponse_ReadoutPerTrigger,
      #                     determination=PicamTriggerDetermination_PositivePolarity,
      #                signalOUT=PicamOutputSignal_NotReadingOut)
     
     cam.add_ROI(0,1340,2,0,100,100)
     print "ROI weird stuff"  
     print cam.get_open_cameras()
     print cam.rois.roi_count
     #print len(cam.rois.roi_array)
     #cam.set_param_value(PicamParameter_ReadoutCount, 5)
     #cam.set_exposure_time(0)
     #frame_tracking= cam.get_param_constraint(PicamParameter_TimeStamps)
     #print "FRAME TRACKING"
     #print frame_tracking
     #print frame_tracking["values_array"].contents
     cam.set_frame_tracking(trackframe= True, trackframebitdepth=64)
     #cam.set_time_stamp(timestamps= PicamTimeStampsMask_ExposureStarted)
     cam.set_time_stamp(timestamps= PicamTimeStampsMask_ExposureEnded)
     #cam.commit_params()
     #print cam.get_IO_settings()
     #print cam.get_param_value(PicamParameter_ReadoutCount)
     #print "Exposure time: ", cam.get_exposure_time()
     #cam.acquire_cont()
     
     sensor_info= cam.get_sensor_info()
     print sensor_info     
     #print sensor_info['Sensor Active Height']
     #print "Constraint"
     #dict_constraint =cam.get_param_constraint(PicamParameter_VerticalShiftRate)
     #print dict_constraint
     #print dict_constraint['values_array'].contents
     
     #print cam.get_sensorcleaning_params()
     
     """  
        I/O SETTINGS
        -   response: set of camera responses to an external trigger (PicamTriggerResponse_NoResponse
            PicamTriggerResponse_ExposeDuringTriggerPulse, PicamTriggerResponse_ReadoutPerTrigger,
            PicamTriggerResponse_ShiftPerTrigger, StartOnSingleTrigger)
        -   determination: external trigger styles that are recognized by a camera (PicamTriggerDetermination_PositivePolarity,
            PicamTriggerDetermination_NegativePolarity, PicamTriggerDetermination_RisingEdge, PicamTriggerDetermination_FallingEdge)
        -   signalOUT: set of parameters defining a camera's MONITOR OUTPUT signal (PIXIS: PicamOutputSignal_NotReadingOut,
            PicamOutputSignal_ShutterOpen, PicamOutputSignal_Busy, PicamOutputSignal_AlwaysLow,
            PicamOutputSignal_AlwaysHigh)
            
        This function only works for PIXIS and QUAD RO cameras. It needs to be adapted for other types of devices.
     """     
     #cam.acquire_cont()
     #data= cam.get_data()
     #print data
     #print cam.available.initial_readout
     #print cam.get_sensorcleaning_params()
     #cam.add_ROI(0, 200, 1, 0, 50, 100)
     #cam.commit_params()
     #cam.add_ROI(250, 400, 1, 51, 70, 1)
    

     
     #camera_count, data = cam.get_open_cameras()
     #cam.get_param_constraint(PicamParameter_AdcBitDepth)
     rois= cam._get_param_value(PicamParameter_Rois)   
     print "ROIs weird stuff"
     #print np.size[rois.roi_array]
     print rois.roi_count
     #rois.roi_array[0].s2=1000
     #print cam.set_param_value(PicamParameter_Rois, rois)
     cam._set_param_value(PicamParameter_ExposureTime, 0)
     cam.set_Adc_params(2, 16, 2, 2.0)
     print cam._commit_params()
     print "acq settings"
     print cam.get_read_only_acquisition_settings()
     #print cam.get_param_value(PicamParameter_Rois)
     #print rois.roi_array[0].s2
     #readout_stride= cam.get_param_value(PicamParameter_ReadoutStride)
     #print readout_stride
     #pixel_bit_depth=cam.get_param_value(PicamParameter_PixelBitDepth)
     #print pixel_bit_depth
     #cam._set_trigger_response(PicamTriggerResponse_NoResponse)
     #print "IO settings"
     #print cam.get_IO_settings()
     #print cam.get_sensorcleaning_params()
     print "param constraint"
     constraint= cam._get_param_constraint(PicamParameter_ReadoutControlMode)     
     print constraint
     print constraint['values_array'].contents
     #cam.set_readout_controls()
     cam._set_param_value(PicamParameter_ReadoutControlMode, 1)
     cam._set_param_value(PicamParameter_VerticalShiftRate, 3.2)

     cam.set_IO_settings(2,1,5)  
     cam.set_shutter_timing(0.0, 0, 1000, 1)
     cam.set_exposure_time(0.001)
     print "shutter stuff"
     print cam._get_param_constraint(PicamParameter_ShutterClosingDelay)
     cam._set_param_value(PicamParameter_ReadoutControlMode, 1)
     cam._set_param_value(PicamParameter_TriggerSource,1)
     cam._commit_params()
     print "trigger source"
     print cam._get_param_value(PicamParameter_TriggerSource)
     #print "IO settings"
     #print cam.get_IO_settings()
     #print "sensor cleaning parameters"
     print cam.get_exposure_time()
     print cam.get_IO_settings()
     print cam.get_sensorcleaning_params()
     print cam.get_Adc_params()
     print cam.get_shutter_timing()
     print cam._get_param_value(PicamParameter_ReadoutControlMode)
     print cam._get_param_constraint(PicamParameter_ReadoutControlMode)
  
     print "Press a key:"
     raw_input()
     
     cam.set_sensorcleaning_params(0, 100, 2, 5, False, False)
     cam._commit_params()
     #print cam._set_output_signal(PicamOutputSignal_NotReadingOut)
     #cam.terminate()
     for i in range(1050):
         print i
         #cam.set_IO_settings(2,1,1)
         #cam._commit_params()
         cam.acquire(-1,200)
         #ele:
         #    cam.acquire(-1,3800-22)
         #cam.set_IO_settings(2,1,4) 
         #cam.commit_params()
         #time.sleep(0.5)
         #ts_res= cam._get_param_value(PicamParameter_TimeStampResolution)
         data, ts= cam.get_data()
         #print type(data)
         filename = 'data/151214/trial_' + str(i) + '.txt'
         #
         np.savetxt(filename, np.squeeze(data), delimiter=' ')
         #np.savetxt('time_stamps.txt', ts)
         #np.savetxt('time_stamps_1Dinterf_201115_test10_' + str(i) + '.txt',np.array(ts)/ts_res)
        
     #data, ts= cam.get_data()

     #for i in range(len(ts) -2):
         #print 1/(ts[i+2] - ts[i+1])
     print cam._get_param_value(PicamParameter_TimeStampResolution)
     print cam.get_read_only_acquisition_settings()
     print cam._get_param_value(PicamParameter_ReadoutTimeCalculation)
     #print cam.get_param_value()   
     #print(np.shape(data))


     #print(data[:,:,0])