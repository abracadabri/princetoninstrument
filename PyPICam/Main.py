"""
    PythonForPicam is a Python ctypes interface to the Princeton Instruments PICAM Library
    Copyright (C) 2013  Joe Lowney.  The copyright holder can be reached at joelowney@gmail.com

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or any 
    later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
"""

"""Test for talking to Picam"""
import ctypes as ctypes
import numpy as np
import sys
import struct
import time
import matplotlib.pyplot as plt
""" Import standard type definitions from PiTypes.py """
from PiTypes import *

""" Import non-standard type definitions from PiTypesMore.py """
from PiTypesMore import *

""" Import function definitions from PiFunctions.py """
""" This should contian all of the function from picam.h """
from PiFunctions import *

""" Import parameter lookup from PiParameterLookup.py """
""" This file includes a function PI_V and a lookup table to return the code
    for different Picam Parameters described in chapter 4 """
from PiParameterLookup import *

############################
##### Custom Functions #####
############################
def get_data(roi_width, roi_height, available_readout, num_frames):
    """ Routine to access initial data.
    Returns numpy array with shape (400,1340) """
    """ Create an array type to hold 1340x400 16bit integers """
    #DataArrayType = pi16u*roi_width*roi_height
    """ Create pointer type for the above array type """
    #print "Type of available_readout ", type(available_readout)
    DataArrayPointerType = ctypes.POINTER(pi16u*(roi_width*1*num_frames))
    """ Create an instance of the pointer type, and point it to initial readout contents (memory address?) """
    DataPointer = ctypes.cast(available_readout,DataArrayPointerType)
    """ Create a separate array with readout contents """
    # TODO, check this stuff for slowdowns
    rawdata = DataPointer.contents
    #data= np.ctypeslib.as_array(rawdata)

    #buffer_from_memory = ctypes.pythonapi.PyBuffer_FromMemory
    #buffer_from_memory.restype = ctypes.py_object
    #buffer = buffer_from_memory(DataPointer, 2*1000)
    data = np.frombuffer(rawdata, dtype='uint16')

    #numpydata = np.frombuffer(rawdata, dtype=np.uint16)
    #data2 = np.reshape(numpydata,(roi_height,roi_width))
    #data=np.copy(data2)# TODO: get dimensions officially,
    # note, the readoutstride is the number of bytes in the array, not the number of elements
    # will need to be smarter about the array size, but for now it works.
    return data
    
def pointer(x):
    """Returns a ctypes pointer"""
    ptr = ctypes.pointer(x)
    return ptr


def load(x):
    """Loads DLL library where argument is location of library"""
    x = ctypes.windll.LoadLibrary(x)
    return x

def PrintData(buf, numframes, framelength):
    
    loop=0
    
    for loop in range(numframes):
        frameptr = buf + ( framelength )
        print(frameptr)
        print(sys.getsizeof(pi16u))
        midpt = frameptr + ( ( ( framelength/sys.getsizeof(pi16u) )/ 2 ) )
        #print( "%5d,%5d,%5d\t%d\n", *(midpt-1), *(midpt), *(midpt+1), loop+1 );
        print(midpt)
        
    return midpt
"""
 void PrintData( pibyte* buf, piint numframes, piint framelength )
{
    pi16u  *midpt = NULL;
    pibyte *frameptr = NULL;

    for( piint loop = 0; loop < numframes; loop++ )
    {
        frameptr = buf + ( framelength * loop );
        midpt = (pi16u*)frameptr + ( ( ( framelength/sizeof(pi16u) )/ 2 ) );
        printf( "%5d,%5d,%5d\t%d\n", *(midpt-1), *(midpt), *(midpt+1), loop+1 );
    }
}  
"""
class PicamRoi(ctypes.Structure):
    pass
PicamRoi._fields_ = [
        ('s1', ctypes.c_int),  #First pixel in the serial register
        ('s2', ctypes.c_int), #Last pixel in the serial register
        ('sbin', ctypes.c_int), #Serial binning for this region 
        ('p1', ctypes.c_int), #First row in the parallel register
        ('p2', ctypes.c_int), #Last row in the parallel register 
        ('pbin', ctypes.c_int)] #Parallel binning for this region 
PicamRoi_ptr=ctypes.POINTER(PicamRoi)
PicamRoi_const_ptr= ctypes.POINTER(PicamRoi)

class PicamRois(ctypes.Structure):
    pass
PicamRois._fields_ = [
        ('roi_array', ctypes.POINTER(PicamRoi)),
        ('roi_count',ctypes.c_int)]
PicamRois_ptr= ctypes.POINTER(PicamRois)

#########################
##### Main Routine  #####

PicamTriggerResponse_NoResponse=1
PicamTriggerResponse_ReadoutPerTrigger=2
PicamTriggerResponse_ShiftPerTrigger=3
PicamTriggerResponse_ExposeDuringTriggerPulse=4
PicamTriggerResponse_StartOnSingleTrigger=5

if __name__ == '__main__':

    """ Load the picam.dll """
    # picamDll = 'C:/Users/becgroup/Documents/Python/DriverTest/Princeton Instruments/Picam/Runtime/Picam.dll'
    picamDll = 'C:\Users\Helene\Documents\Work\McGill\PhD\Code\PrincetonInstrument\PythonForPicam-master2\Picam.dll'
    #piccDll = 'C:\Program Files\Princeton Instruments\Picam\Runtime\Picc.dll'
    #pidaDll = 'C:\Program Files\Princeton Instruments\Picam\Runtime\Pida.dll'
    #pidiDll = 'C:\Program Files\Princeton Instruments\Picam\Runtime\Pidi.dll'
    picam = load(picamDll)
    #picc=load(piccDll)
    #pida=load(pidaDll)
    #pidi=load(pidiDll)
    
    is_initialized= ctypes.c_bool()
    print 'successfuly loaded the dll'
    print 'Is library Initialized (Yes is True): ', is_initialized.value
    print 'Initialize Camera.',Picam_InitializeLibrary()
    Picam_IsLibraryInitialized(ctypes.byref(is_initialized))
    print 'Is library Initialized (Yes is True): ', is_initialized.value
    print '\n'

    """ Print version of PICAM """
    major = piint()
    minor = piint()
    distribution = piint()
    release = piint()
    print 'Check Software Version. ',Picam_GetVersion(pointer(major),pointer(minor),pointer(distribution),pointer(release))
    print 'Picam Version ',major.value,'.',minor.value,'.',distribution.value,' Released: ',release.value
    print '\n'


    model = ctypes.c_int(10)
    serial_number = ctypes.c_char_p('Demo Cam 1')
    PicamID = PicamCameraID()  
    PicamFirmware = PicamFirmwareDetail()
    camera = PicamHandle()
    string=ctypes.c_char_p()
    connected=ctypes.c_bool()
    PicamHandle_ptr= ctypes.POINTER(PicamHandle)
    picamhandle_ptr= PicamHandle_ptr()
    camera_count= piint()
    is_demo=ctypes.c_bool()
    
    open_elsewhere=ctypes.c_bool()
    Picam_IsCameraConnected(camera, ctypes.byref(connected))
    print 'Is camera connected (True is Yes): ', connected.value
    Picam_GetOpenCameras(ctypes.byref(picamhandle_ptr), ctypes.byref(camera_count))
    print 'Get Open Cameras: ', camera_count.value
    Picam_IsCameraIDOpenElsewhere(ctypes.byref(PicamID), ctypes.byref(open_elsewhere))
    print 'Is Camera ID opened elsewhere (True if Yes): ', open_elsewhere.value 
    print 'Camera Handle value: ', camera.value
    print 'Opening First Camera: ', Picam_OpenFirstCamera(ctypes.pointer(camera))
    print 'Camera Handle value: ', camera.value
    Picam_IsCameraConnected(camera, ctypes.byref(connected))
    print 'Is camera connected (True is Yes): ', connected.value
    Picam_GetOpenCameras(ctypes.byref(picamhandle_ptr), ctypes.byref(camera_count))
    print 'Get Open Cameras: ', camera_count.value, picamhandle_ptr.contents
    print 'Destroy handle: ', Picam_DestroyHandles(picamhandle_ptr)
    Picam_IsCameraIDOpenElsewhere(ctypes.byref(PicamID), ctypes.byref(open_elsewhere))
    print 'Is Camera ID opened elsewhere (True if Yes): ', open_elsewhere.value 
    Picam_IsDemoCamera(ctypes.byref(PicamID), ctypes.byref(is_demo))
    print 'Is Camera ID that of a demo camera (True if Yes):', is_demo.value
    print '\n'
    Picam_GetCameraID( camera, ctypes.byref(PicamID))    
    Picam_GetEnumerationString( PicamEnumeratedType_Model, PicamID.model, ctypes.byref(string) )
    print "Camera model: ", string.value 
    print "Camera Computer interface is: ", PicamID.computer_interface
    print "Serial number: ", PicamID.serial_number
    print "Sensor name: ", PicamID.sensor_name
    Picam_DestroyString( string )
    print '\n'

    """
    Testing out Firmware info functions
    """
    PicamFirmware_ptr= ctypes.pointer(PicamFirmware)
    Firmware_count= piint()
    print 'Picam_GetFirmwareDetails: ', Picam_GetFirmwareDetails(ctypes.byref(PicamID), ctypes.byref(PicamFirmware_ptr), ctypes.byref(Firmware_count))
    print 'Firmware name: ', PicamFirmware_ptr.contents.name
    print 'Firmware details: ', PicamFirmware_ptr.contents.detail  
    print 'Picam_DestroyFirmwareDetails: ', Picam_DestroyFirmwareDetails(PicamFirmware_ptr)
    print '\n'
    
    """
    Checking for Demo Cameras    
    """ 
    
    PicamModel_ptr= ctypes.POINTER(PicamModel*133)
    DemoModels=PicamModel_ptr()
    demo_count=piint()
    # 6 corresponds to PIXIS 100B
    model = ctypes.c_int(6)
    serial_number = ctypes.c_char_p('12345')
    PicamID_Demo = PicamCameraID()
    Picam_GetAvailableDemoCameraModels(ctypes.byref(DemoModels), ctypes.byref(demo_count))
    print 'Available Demo Cameras: ', demo_count.value
    print 'Open Demo Cameras: ', Picam_ConnectDemoCamera(model, ctypes.byref(serial_number), ctypes.byref(PicamID_Demo))
    
    print 'Picam_DestroyModels: ', Picam_DestroyModels(DemoModels)
    print '\n'
    
    
    Picam_IsDemoCamera(ctypes.byref(PicamID_Demo), ctypes.byref(is_demo))
    print 'Is Camera ID that of a demo camera (True if Yes):', is_demo.value
    print '\n'
    Picam_GetCameraID( camera, ctypes.byref(PicamID_Demo))    
    Picam_GetEnumerationString( PicamEnumeratedType_Model, PicamID_Demo.model, ctypes.byref(string) )
    print "Camera model: ", string.value 
    print "Camera Computer interface is: ", PicamID_Demo.computer_interface
    print "Serial number: ", PicamID_Demo.serial_number
    print "Sensor name: ", PicamID_Demo.sensor_name
    Picam_DestroyString( string )
    print '\n'
    #print 'Disconnect Demo Camera: ', Picam_DisconnectDemoCamera(PicamID_Demo)
    
    
    
    print 'SETTING AND GETTING PARAMETERS'
    print '\n'

    available=PicamAvailableData()
    errors=PicamAcquisitionErrorsMask()
    
    #Reports the length, in bytes, necessary to traverse to the next readout.
    readoutstride=piint()
    print 'Getting readout stride: ', Picam_GetParameterIntegerValue( camera, PicamParameter_ReadoutStride, ctypes.byref(readoutstride))
    print 'Readout stride value: ', readoutstride.value   
    print '\n'
    
    """
    Exposure time settings
    """
    exposure_time=ctypes.c_double()
    #print(type(exposure_time))
    #print(exposure_time)
    print "Getting exposure time: ", Picam_GetParameterFloatingPointValue(camera, ctypes.c_int(PicamParameter_ExposureTime), ctypes.byref(exposure_time) );
    print "Exposure time before setting [in ms?]: ",  exposure_time.value
    print "Setting exposure time: ", Picam_SetParameterFloatingPointValue(camera, ctypes.c_int(PicamParameter_ExposureTime), ctypes.c_double(0) );
    print "Getting readout stride: ", Picam_GetParameterFloatingPointValue(camera, ctypes.c_int(PicamParameter_ExposureTime), ctypes.byref(exposure_time) );
    print "Exposure time after setting [in ms?]: ",  exposure_time.value
    print '\n'
    
    
    """
    ROIS settings
    """

    region_array=PicamRois_ptr()
 
    print "Getting ROI", Picam_GetParameterRoisValue(camera,ctypes.c_int(PicamParameter_Rois), ctypes.byref(region_array ))
    print 'number of regions: ', region_array.contents.roi_count 
    print 'start pixel (serial):', region_array.contents.roi_array[0].s1
    print 'end pixel (serial):', region_array.contents.roi_array[0].s2
    print 'pixel binning (serial):', region_array.contents.roi_array[0].sbin
    print 'start pixel (parallel):', region_array.contents.roi_array[0].p1
    print 'end pixel (parallel):', region_array.contents.roi_array[0].p2
    print 'pixel binning (parallel):', region_array.contents.roi_array[0].pbin
    print '\n'
    
    
    region_array.contents.roi_array[0].s2=1340
    region_array.contents.roi_array[0].p2 = 100
    region_array.contents.roi_array[0].pbin=100
    
    print "Setting ROI", Picam_SetParameterRoisValue(camera,ctypes.c_int(PicamParameter_Rois),ctypes.byref(region_array.contents) )
    print "Getting ROI again", Picam_GetParameterRoisValue(camera,ctypes.c_int(PicamParameter_Rois), ctypes.byref(region_array) )
    print 'number of regions: ', region_array.contents.roi_count 
    print 'start pixel (serial):', region_array.contents.roi_array[0].s1
    print 'end pixel (serial):', region_array.contents.roi_array[0].s2
    print 'pixel binning (serial):', region_array.contents.roi_array[0].sbin
    print 'start pixel (parallel):', region_array.contents.roi_array[0].p1
    print 'end pixel (parallel):', region_array.contents.roi_array[0].p2
    print 'pixel binning (parallel):', region_array.contents.roi_array[0].pbin
    print '\n'
    
    
    """
    TRIGGER settings
    """
    trigger_response=PicamEnumeratedType()
    print 'Checks parameter type: ', Picam_GetParameterEnumeratedType( camera, PicamParameter_TriggerResponse , ctypes.byref(trigger_response))
    print Picam_GetParameterIntegerValue( camera, PicamParameter_TriggerResponse, ctypes.byref(trigger_response) );
    print 'trigger response: ',trigger_response.value
    print Picam_SetParameterIntegerValue( camera, PicamParameter_TriggerResponse,2 );
    print Picam_GetParameterIntegerValue( camera, PicamParameter_TriggerResponse, ctypes.byref(trigger_response) );
    print 'trigger response: ',trigger_response.value
    print '\n'
  
    """
    SHUTTER settings
    """
    shutter_timing_mode=PicamEnumeratedType()    
    print Picam_GetParameterIntegerValue( camera, PicamParameter_ShutterTimingMode, ctypes.byref(shutter_timing_mode) );
    print 'shutter timing mode: ',shutter_timing_mode.value
    print Picam_SetParameterIntegerValue( camera, PicamParameter_ShutterTimingMode, 1 );
    print Picam_GetParameterIntegerValue( camera, PicamParameter_ShutterTimingMode, ctypes.byref(shutter_timing_mode) );
    print 'shutter timing mode: ',shutter_timing_mode.value
    print '\n'
    
    """
    ORIENTATION settings (Read Only)
    Note: what is the difference betwen PicamParameter_Orientation and PicamParameter_ReadoutOrientation?
    """
    readout_orientation=PicamEnumeratedType()    
    print Picam_GetParameterIntegerValue( camera, PicamParameter_ReadoutOrientation, ctypes.byref(readout_orientation) );
    print 'Readout orientation: ',readout_orientation.value
    print '\n'
    
    """
    READOUT CONTROL settings
    """
    readout_control_mode=PicamEnumeratedType()    
    print Picam_GetParameterIntegerValue( camera, PicamParameter_ReadoutControlMode, ctypes.byref(readout_control_mode) );
    print 'Readout control mode: ',readout_control_mode.value
    #print Picam_SetParameterIntegerValue( camera, PicamParameter_ReadoutControlMode, 1 );
    print Picam_GetParameterIntegerValue( camera, PicamParameter_ReadoutControlMode, ctypes.byref(readout_control_mode) );
    print 'Readout control mode: ',readout_control_mode.value
    print '\n'
    
    """
    Data Formatting check settings
    """
    formatting_disabled=ctypes.c_bool(True)
    print Picam_GetParameterIntegerValue( camera, PicamParameter_DisableDataFormatting , ctypes.byref(formatting_disabled) );
    print 'Data formatting disabled: ',formatting_disabled.value
    print '\n'
    
    
    """
    FRAME parameters
    """
    frame_size=ctypes.c_int()  
    print Picam_GetParameterIntegerValue( camera, PicamParameter_FrameSize, ctypes.byref(frame_size) );
    print 'Frame size: ',frame_size.value
    frame_stride=ctypes.c_int()  
    print Picam_GetParameterIntegerValue( camera, PicamParameter_FrameStride, ctypes.byref(frame_stride) );
    print 'Frame stride: ',frame_stride.value
    frame_per_readout=ctypes.c_int()  
    print Picam_GetParameterIntegerValue( camera, PicamParameter_FramesPerReadout, ctypes.byref(frame_per_readout) );
    print 'Frame per readout: ',frame_per_readout.value
    frame_rate_calc=ctypes.c_double()  
    print Picam_GetParameterFloatingPointValue( camera, PicamParameter_FrameRateCalculation, ctypes.byref(frame_rate_calc) );
    print 'Frame rate calculation: ', frame_rate_calc.value
    print '\n'
    
    """
    ADC Settings Parameters
    """    
    adc_speed= ctypes.c_double()
    print Picam_GetParameterFloatingPointValue(camera, PicamParameter_AdcSpeed, ctypes.byref(adc_speed))
    print "Adc speed:", adc_speed.value
    
    
    adc_bitdepth= ctypes.c_int()
    print Picam_GetParameterIntegerValue(camera, PicamParameter_AdcBitDepth, ctypes.byref(adc_bitdepth))
    print "Adc bit depth:", adc_bitdepth.value
    
    adc_gain= ctypes.c_int()
    print Picam_GetParameterIntegerValue(camera, PicamParameter_AdcAnalogGain, ctypes.byref(adc_gain))
    print Picam_SetParameterIntegerValue( camera, PicamParameter_AdcAnalogGain, 3 );
    print "Adc gain is:", adc_gain.value
    
    adc_quality= ctypes.c_int()
    print Picam_GetParameterIntegerValue(camera, PicamParameter_AdcQuality, ctypes.byref(adc_quality))
    print "Adc quality is:", adc_quality.value
    
        
        
    
    paramsFailed=ctypes.pointer((ctypes.c_int)())
    failCount=piint()
    are_committed=ctypes.c_bool(0)
    Picam_AreParametersCommitted(camera,ctypes.byref(are_committed))
    print "Checking if parameters are committed (True if yes):", are_committed.value
    print "committing param:", Picam_CommitParameters(camera, ctypes.byref(paramsFailed), ctypes.byref(failCount))
    print "no of param which failed committing:", failCount.value
    Picam_AreParametersCommitted(camera,ctypes.byref(are_committed))
    print "Checking if parameters are committed (True if yes):", are_committed.value
    print '\n'
    print "Adc gain is:", adc_gain.value
    #param_arrayType = ctypes.c_long*100
    
    param_array_type= ctypes.POINTER(ctypes.c_int*100)
    param_array=param_array_type()    
    param_count=piint()
    print "Getting all parameters:", Picam_GetParameters(camera, ctypes.byref(param_array),ctypes.byref(param_count))
    print "Number of parameters found:", param_count.value
    print "parameters", len(param_array.contents)
    print '\n' 
    
    print 'Getting readout stride: ', Picam_GetParameterIntegerValue( camera, PicamParameter_ReadoutStride, ctypes.byref(readoutstride))
    print 'Readout stride value: ', readoutstride.value   
    print '\n' 
									
    print("Destroy parameters params Failed:",Picam_DestroyParameters(paramsFailed))
    print("Destroy parameters param_array:",Picam_DestroyParameters(param_array))
    print '\n'
    time.sleep(1)

 
    
    """
    print 'Start Acquisition', Picam_StartAcquisition(camera)
    status= PicamAcquisitionStatus()
    status.running=True
    while status.running==True:
        time.sleep(2)
        print 'Wait for acquisition update: ', Picam_WaitForAcquisitionUpdate(camera, piint(1000), ctypes.byref(available), ctypes.byref(status))
        print 'Is running: ', status.running
        print 'Error:', status.errors
        print 'Readout Rate: ', status.readout_rate
        print available.initial_readout
        print type(available.initial_readout)
        print available.readout_count
        
        if status.running==False:
            print 'Problem acquiring: ', Picam_WaitForAcquisitionUpdate(camera, piint(1000), ctypes.byref(available), ctypes.byref(status))  
    
    print 'Stop Acquisition', Picam_StopAcquisition( camera )
    """   
    print(available.initial_readout, available.readout_count)
    readout_count = pi64s(10)
    NO_TIMEOUT= -1
    Picam_Acquire.argtypes = PicamHandle, pi64s, piint, ctypes.POINTER(PicamAvailableData), ctypes.POINTER(PicamAcquisitionErrorsMask)
    Picam_Acquire.restype = piint
    print "Acquiring: ", Picam_Acquire( camera, readout_count, piint(NO_TIMEOUT), ctypes.byref(available), ctypes.byref(errors))     
    print "Available initial_readout: ", available.initial_readout
    print "Readout_count value: ", readout_count.value
    print "Readout stride: ", readoutstride.value
    print(type(available.initial_readout), type(available.readout_count))
    is_running=ctypes.c_bool()
    Picam_IsAcquisitionRunning(camera, ctypes.byref(is_running))
    print "Is acquisition running:", is_running.value
    time.sleep(1)
    print "errors", errors
    print '\n'
    
    readout_time= ctypes.c_double(1)
    Picam_GetParameterFloatingPointValue(camera, PicamParameter_ReadoutTimeCalculation, ctypes.byref(readout_time))
    print "Readout time:", readout_time.value
    


 
    data=get_data(region_array.contents.roi_array[0].s2, region_array.contents.roi_array[0].p2, available.initial_readout, readout_count.value )
    print type(data)
    print len(data)
    print data
    #print data.shape
    #print data.size
    
    
    plt.plot(data)
    plt.show()
    
    """
    size_buffer= 0.5*readoutstride.value #*readout_count.value
    DataArrayType = pi16u*int(1340*100)
    #Create an instance of the pointer type, and point it to initial readout contents (memory address?)
    DataArrayPointerType = ctypes.POINTER(DataArrayType)    
    DataPointer = ctypes.cast(available.initial_readout,DataArrayPointerType)
    data=DataPointer.contents
    dat= np.frombuffer(data, dtype='uint16')
    #plt.plot(dat)
    #plt.show()
    """
    #print(dat)

    """ Close out Library Resources """
    ## Disconnected the above cameras
    print 'Disconnecting demo camera', Picam_CloseCamera(camera)
    ## Close down library    
    print 'Uninitializing',Picam_UninitializeLibrary()    
   