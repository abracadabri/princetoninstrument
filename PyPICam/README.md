PyPICam
==============


PyPICam is an attempt to provide a Python 2.7 wrapper module on Princeton Instument C-library Picam.dll used to interface with the following 
hardware on 64bits platforms: NIRvana ST, NIRvana LN, PI-MAX 3 and 4, PI-MTE, PIoNIR/NIRvana, PIXIS, ProEM/ProEM+, PyLoN, PyLoN-IR
Quad-RO

The code's structure is directly and heavily inspired from the pvcam.h module developped by Eric Piel for the pvcam.dll
library on 32bits platforms. It also inspires itself from Joe Lowney and Andrew Dawes' codes.

The code has only been tested on Windows with the PI PIXIS 100B (USB) camera.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.


Requirements
----------------------------------------

- Python 2.7
- Picam.dll, Picc.dll, Pida.dll, Pidi.dll available for free here: ftp://ftp.princetoninstruments.com/Public/Software/Official/PICam/


Available modules
----------------------------------------
- picam.py: 
- picam_h.py:
- picam_advanced_h.py: 
- test.py: validation tests. Can be used to understand how methods work.


Contributors
----------------------------------------
Helene Seiler (helene.seiler@mail.mcgill.ca)
